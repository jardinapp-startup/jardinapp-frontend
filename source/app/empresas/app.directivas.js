(function() {
    'use strict';

    angular.module('app.empresa.directivas',[
    ])
    .directive('empresas', Empresas)
    .directive('empresascreate', EmpresasCreate)
    .directive('empresasupdate', EmpresasUpdate);
    function Empresas(){
      return {
        scope: {},
        restrict:'EA',
        templateUrl: 'app/empresas/lista.html',
        controller: 'EmpresasCtrl',
        controllerAs: 'vm'
      }
    }

    function EmpresasCreate(){
      return {
        scope: {},
        restrict:'EA',
        templateUrl: 'app/empresas/create.html',
        controller: 'EmpresasCreate',
        controllerAs: 'vm'
      }
    }
     function EmpresasUpdate(){
      return {
        scope: {},
        restrict:'EA',
        templateUrl: 'app/empresas/update.html',
        controller: 'EmpresasUpdateCtrl',
        controllerAs: 'vm'
      }
    }

})();
