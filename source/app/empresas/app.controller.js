(function() {
    'use strict';

    angular.module('app.empresas.controller', [
    ])
    .controller('EmpresasCtrl', EmpresasCtrl)
    .controller('EmpresasCreate',EmpresasCreate)
    .controller('EmpresasUpdateCtrl',EmpresasUpdateCtrl);
    //Lista
    EmpresasCtrl.$inject = ['empresasServices', 'SecurityFilter','$location'];

    function EmpresasCtrl(empresasServices, SecurityFilter,$location){
      var vm=this;
      if(SecurityFilter.isEmpresario()){
          vm.empresa = empresasServices.query();
      }else{
        $location.path('/');
      }
      vm.onChange = function (id) {
          empresasServices.inhabilit({idEmpresa:id}).$promise
          .then(function(data){
          })
          .catch(function(data){
          });
      }
    }
    //create
    EmpresasCreate.$inject = ['$location', 'SecurityFilter','empresasServices','Ciudades','$mdToast'];
    function EmpresasCreate($location, SecurityFilter,empresasServices,Ciudades,$mdToast){
     var vm=this;
      if(!SecurityFilter.isAuthenticated()){
           $location.path('/');
      }else{
      vm.queryCiudades = queryCiudades;
      }
      vm.create = function() {
        empresasServices.save(vm.empresa, function(){
          $mdToast.show(
            $mdToast.simple()
            .textContent('la empresa fue creado exitosamente')
            .position('bottom right'));
          $location.path('/empresas');
        });
        //Con el save realiza un método POST
      };

      function queryCiudades(str){
        return Ciudades.queryByNombre({
          query: str
        });
      }
    }
    //update
    EmpresasUpdateCtrl.$inject = ['$location','SecurityFilter','empresasServices','$stateParams','$mdToast'];
    function EmpresasUpdateCtrl($location,SecurityFilter,empresasServices,$stateParams,$mdToast){
      var vm=this;

      if (!SecurityFilter.isEmpresario()) {

          $location.path('/');
      }
       vm.id = $stateParams.idempresa;
       vm.empresa = empresasServices.get({ idempresa: vm.id});
       vm.update = function() {
           empresasServices.update(vm.empresa, function(){
           $mdToast.show(
                    $mdToast.simple()
                        .textContent('la empresa fue actualizado exitosamente')
                        .position('bottom right'));
                $location.path('/empresa');
            });
       }

    }
})();
