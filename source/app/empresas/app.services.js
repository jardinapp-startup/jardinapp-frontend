(function () {
    'use strict';

    angular.module('app.empresas.services', [

    ])
        .factory('empresasServices', empresasServices)
        .factory('Ciudades', Ciudades);

    empresasServices.$inject = ['$resource','BASEURL'];

    function empresasServices($resource,BASEURL) {
       return $resource(BASEURL+'/empresas/:idempresa',
       { idempresa :'@idempresa'},
       { 'update':{method:'PUT'},
       inhabilit: {
         url: BASEURL+'/empresas/inhabilit/:idEmpresa',
         method: 'PUT',
         params: {idEmpresa: '@idEmpresa'}
       },

       });
     }


    Ciudades.$inject = ['$resource', 'BASEURL'];

        function Ciudades($resource, BASEURL) {
          return $resource(BASEURL + '/ciudades/:idCiudad', {
            idCiudad: '@idCiudad'
          },{
            queryByNombre: {
              url: BASEURL + '/ciudades/nombre/:query',
              method: 'GET',
              isArray: true,
              params: {
                query: '@query'
              }
            }
          })
        }

})();
