(function(){
    'use strict';

    angular.module('app.empresas',[
      'app.empresas.services',
      'app.empresas.controller',
      'app.empresa.directivas',
      'app.empresas.router'
    ]);
})();
