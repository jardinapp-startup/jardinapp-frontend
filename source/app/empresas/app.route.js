(function(){
    'use strict';

    angular.module('app.empresas.router', [
    ])
        .config(configure);

    configure.$inject = ['$stateProvider', '$urlRouterProvider'];

    function configure($stateProvider, $urlRouterProvider) {

      $urlRouterProvider.otherwise('/');

      $stateProvider
          .state('empresas', {
              url: '/empresas',
              template: '<empresas/>'
          })
          .state('empresascreat',{
            url:'/empresas/create',
            template: '<empresascreate/>'
          })
          .state('empresasupd',{
            url:'/empresas/update/:idempresa',
            template: '<empresasupdate/>'
          })
    };
})();
