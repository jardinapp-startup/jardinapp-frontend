(function () {
    'use strict';

    angular.module('app.departamentos.controller', [
    ])
    .controller('departamentosCtrl', departamentosCtrl);

   departamentosCtrl.$inject = ['departamentosServices'];

    function departamentosCtrl(departamentosServices) {
        this.departamentos = departamentosServices.query();
    }

})();
