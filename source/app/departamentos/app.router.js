(function () {
    'use strict';

    angular.module('app.departamentos.router', [

    ])
        .config(configure);


    configure.$inject = ['$stateProvider', '$urlRouterProvider'];

    function configure($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('departamentos', {
                url: '/departamentos',
                views: {
                    'departamentos': {
                        template: '<departamentos/>'
                    },
                    'piepagina': {
                        template: '<piepagina/>'
                    },
                    'encabezado': {
                        template: '<encabezado/>'
                    }
                }
            });
    };
})();
