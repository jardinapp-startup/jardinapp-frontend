(function () {
    'use strict';

    angular.module('app.departamentos', [
        'app.departamentos.directivas',
        'app.departamentos.controller',
        'app.departamentos.services',
        'app.departamentos.router'
    ]);

})();
