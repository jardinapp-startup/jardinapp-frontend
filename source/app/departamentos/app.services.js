(function () {
    'use strict';

    angular.module('app.departamentos.services', [

    ])
        .factory('departamentosServices', departamentosServices);

    departamentosServices.$inject = ['$resource','BASEURL'];

    function departamentosServices($resource,BASEURL) {
       return $resource(BASEURL+'/departamentos/:id');
    }

})();
