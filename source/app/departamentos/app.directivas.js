(function () {
    'use strict';

    angular.module('app.departamentos.directivas', [

    ]).directive('departamentos', departamentos);

    function departamentos() {
        return {
            scope: {},
            templateUrl: 'app/departamentos/list.html',
            controller: 'departamentosCtrl',
            controllerAs: 'vm'
        }
    }

})();
