(function () {
    'use strict';

    angular.module('app.actividades.directivas', [

    ]).directive('actividades', actividades)
        .directive('colorear', colorear)
        .directive('geometricas', geometricas)
        .directive('numeros', numeros)
        .directive('vocales', vocales)
        .directive('reciclaje', reciclaje);

    function actividades() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/actividades/actividades.html',
            controller: 'actividadesCtrl',
            controllerAs: 'vm'
        };
    }

    function colorear() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/actividades/colorear.html',
            controller: 'actividadesCtrl',
            controllerAs: 'vm'
        };
    }
    function geometricas() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/actividades/geometricas.html',
            controller: 'actividadesCtrl',
            controllerAs: 'vm'
        };
    }

    function numeros() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/actividades/numeros.html',
            controller: 'actividadesCtrl',
            controllerAs: 'vm'
        };
    }

    function vocales() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/actividades/vocales.html',
            controller: 'actividadesCtrl',
            controllerAs: 'vm'
        };
    }
    function reciclaje() {
      return {
          scope: {},
          restrict: 'EA',
          templateUrl: 'app/actividades/reciclaje.html',
          controller: 'actividadesCtrl',
          controllerAs: 'vm'
      };
 }



})();
