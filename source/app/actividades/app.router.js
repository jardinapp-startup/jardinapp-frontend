(function () {
    'use strict';

    angular.module('app.actividades.router', [

    ])
        .config(configure);


    configure.$inject = ['$stateProvider', '$urlRouterProvider'];


    function configure($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('actividades', {
                url: '/actividades',
                template: '<actividades/>'
            })
            .state('colorear', {
                url: '/colorear',
                template: '<colorear/>'
            })
            .state('geometricas', {
                url: '/geometricas',
                template: '<geometricas/>'
            })
            .state('numeros', {
                url: '/numeros',
                template: '<numeros/>'
            })

            .state('vocales', {
                url: '/vocales',
                template: '<vocales/>'
            })
            .state('reciclaje', {
              url: '/reciclaje',
              template: '<reciclaje/>'
          })
    };
})();
