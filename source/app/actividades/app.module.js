(function () {
    'use strict';

    angular.module('app.actividades', [
        'app.actividades.router',
        'app.actividades.directivas',
        'app.actividades.controller',
        'app.actividades.services'
    ]);

})();
