(function () {
    'use strict';

    angular.module('app.actividades.services', [

    ])
        .factory('actividadesServices', actividadesServices)


    actividadesServices.$inject = ['$resource','BASEURL'];

    function actividadesServices($resource,BASEURL) {
       return $resource(BASEURL+'/actividades');
    }

})();
