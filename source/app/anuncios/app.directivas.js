(function () { 
    'use strict';

    angular.module('app.anuncios.directivas', [
 
    ]).directive('anuncios', anuncios)
    .directive('anuncioscreate', anunciosCreate)
    .directive('anunciosupdate', anunciosUpdate)
    .directive('anuncioslistadminanuncio', anunciosListAdminAnuncio)
    .directive('anuncioslistadminj', anunciosListAdminj);

    function anuncios() {
        return {
            scope: {},
            restrict:'EA',
            templateUrl: 'app/anuncios/list.html',
            controller:  'anunciosCtrl',
            controllerAs: 'vm'
        }
    }

    function anunciosCreate() {
      return {
          scope: {},
          restrict:'EA',
          templateUrl: 'app/anuncios/create.html',
          controller:  'anunciosCreate',
          controllerAs: 'vm'
      }
    }

    function anunciosUpdate() {
      return {
          scope: {},
          restrict:'EA',
          templateUrl: 'app/anuncios/update.html',
          controller:  'anunciosUpdate',
          controllerAs: 'vm'
      }
    }
    
    
    function anunciosListAdminAnuncio() {
      return {
          scope: {},
          restrict:'EA',
          templateUrl: 'app/anuncios/listaAdminAnuncio.html',
          controller:  'anunciosListAdminAnuncioCtrl', 
          controllerAs: 'vm'
      }
    }
     function anunciosListAdminj() {
      return {
          scope: {},
          restrict:'EA',
          templateUrl: 'app/anuncios/listaAdminJ.html',
          controller:  'anunciosListAdminJCtrl', 
          controllerAs: 'vm'
      }
    }
    
})();

