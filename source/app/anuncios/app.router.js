(function () {
    'use strict';

    angular.module('app.anuncios.router', [

    ])
        .config(configure);

    //Se inyecta los parametros
    configure.$inject = ['$stateProvider', '$urlRouterProvider'];

    //Se configura las rutas de la aplicación para modelo
    function configure($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('anuncios', {
                url: '/anuncios',
                template: '<anuncios/>'
            })
            .state('anuncioscreate', {
                url: '/anuncios/create',
                template: '<anuncioscreate/>'
            })
            .state('anunciosupdate', {
                url: '/anuncios/update/:idAnuncio',
                template: '<anunciosupdate/>'
            })
             .state('anunciosListaAdminAnuncio', {
                url: '/anuncios/listadminanuncio',
                template: '<anuncioslistadminanuncio/>'
            })
              .state('anunciosListaAdminJ', {
                url: '/anuncios/listadminj',
                template: '<anuncioslistadminj/>'
            })
      };
})();
