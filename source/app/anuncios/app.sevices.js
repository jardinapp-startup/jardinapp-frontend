(function () {
    'use strict';

    angular.module('app.anuncios.services', [

    ])
     .factory('anunciosServices', anunciosServices);

    anunciosServices.$inject = ['$resource','BASEURL'];


    //Este servicio nos provee de los métodos GET POST PUT DELETE
    function anunciosServices($resource,BASEURL) {
       return $resource(BASEURL+'/anuncios/:idAnuncio',
       { idAnuncio: '@idAnuncio' },
       { 'update': {method: 'PUT'},
          anunciosMiJardin:{
            url:BASEURL+'/anuncios/anunciosmijardin',
            method:'GET',
            isArray:true
          },
          inhabilit: {
             url:BASEURL+'/anuncios/inhabilit/:idAnuncio',
             method: 'PUT',
             params:{idAnuncio: '@idAnuncio'} 
         },

         listaAdminJardin:{
             url:BASEURL+'/anuncios/listaAdminJardin',
             method: 'GET',
             isArray:true
         }
       });
    }
})();
