(function () {
    'use strict';

    angular.module('app.anuncios.controller', [
    ])
    .controller('anunciosCtrl', anunciosCtrl)
    .controller('anunciosCreate', anunciosCreate)
    .controller('anunciosUpdate', anunciosUpdate)
    .controller('anunciosListAdminAnuncioCtrl', anunciosListAdminAnuncioCtrl)
    .controller('anunciosListAdminJCtrl', anunciosListAdminJCtrl)

    anunciosCtrl.$inject = ['anunciosServices', 'SecurityFilter'];

    function anunciosCtrl(anunciosServices, SecurityFilter) {

        this.anuncios = anunciosServices.anunciosMiJardin();
        this.isAdmin=function(){
          return SecurityFilter.isAdmin();
        }
      }

    //Create
    anunciosCreate.inject = ['$location', 'anunciosServices', '$mdToast','SecurityFilter','jardinesinfantilesServices'];

    function anunciosCreate($location, anunciosServices, $mdToast,SecurityFilter,jardinesinfantilesServices) {
      if(!SecurityFilter.isAuthenticated()){

        $location.path("/");
      };
       var vm=this;
      jardinesinfantilesServices.getMiJardin().$promise
     .then(function(data){
      vm.jardines = data;
     })
      
      vm.create = function() {
        anunciosServices.save(vm.anuncios, function(){
          $mdToast.show(
            $mdToast.simple()
            .textContent('El anuncio fue creado exitosamente')
            .position('bottom right'));
          $location.path('/anuncios/listadminj');
        });
        //Con el save realiza un método POST
      }
  }
    //Update
  anunciosUpdate.$inject = ['$stateParams', '$location', 'anunciosServices', '$mdToast','SecurityFilter','jardinesinfantilesServices'];

    function anunciosUpdate($stateParams, $location, anunciosServices, $mdToast,SecurityFilter,jardinesinfantilesServices) {
      if(!SecurityFilter.isAuthenticated() && !SecurityFilter.isAdminJardin() && !SecurityFilter.isAdmin()){

        $location.path("/");
      }
      var vm=this;
      vm.onChange = function (id) {

       anunciosServices.inhabilit({idAnuncio:id}).$promise
       .then(function(data){

           console.log(data);
       })
       .catch(function(data){

           console.log(data);
       })
     };
     jardinesinfantilesServices.getMiJardin().$promise
     .then(function(data){
      vm.jardines = data;
     })
        vm.id = $stateParams.idAnuncio;
        vm.anuncios = anunciosServices.get({ idAnuncio: vm.id });
         console.log(vm.anuncios);
        vm.update = function() {
            anunciosServices.update(vm.anuncios).$promise
            .then(function(data){
              $mdToast.show(
                $mdToast.simple()
                .textContent('El anuncio fue actualizado exitosamente')
                .position('bottom right'));
                if(SecurityFilter.isAdmin()){
                  $location.path('/anuncios/listadminanuncio');

                }else if (SecurityFilter.isAdminJardin()) {
                  $location.path('/anuncios/listadminj');
                }
            })
            .catch(function(err){
              console.log(err);
            })
        }

    }


    anunciosListAdminAnuncioCtrl.$inject = ['$location','SecurityFilter','anunciosServices']
    function anunciosListAdminAnuncioCtrl($location,SecurityFilter, anunciosServices){
        if(SecurityFilter.isAdmin()){
        this.anuncios = anunciosServices.query();
      }else{

        $location.path('/');
      }

   }
   anunciosListAdminJCtrl.$inject = ['$location','SecurityFilter','anunciosServices']
    function anunciosListAdminJCtrl($location,SecurityFilter, anunciosServices){
      if(SecurityFilter.isAdminJardin()){
        this.anuncios = anunciosServices.listaAdminJardin();
      }else{

        $location.path('/');
      }

       this.onChange = function (id) {

        anunciosServices.inhabilit({idAnuncio:id}).$promise
        .then(function(data){
            console.log("EXITO");
            console.log(data);
        })
        .catch(function(data){
            console.log("NO EXITO");
            console.log(data);
        })
      };

   }

})();
