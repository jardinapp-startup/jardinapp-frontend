(function(){
    'use strict';
    
    angular.module('app.anuncios',[
        'app.anuncios.directivas', 
        'app.anuncios.router',
        'app.anuncios.services',
        'app.anuncios.controller'
    ])
})();