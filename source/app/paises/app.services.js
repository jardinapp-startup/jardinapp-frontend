(function () {
    'use strict';

    angular.module('app.paises.services', [

    ])
        .factory('paisesServices', paisesServices);

    paisesServices.$inject = ['$resource','BASEURL'];


    //Este servicio nos provee de los métodos GET POST PUT DELETE
    function paisesServices($resource,BASEURL) {
       return $resource(BASEURL+'/paises/:id');
    }

    //De igual manera los factory nos sirven para almacenar información
    //y que nos pueda servir en cualquier controlador o lugar de la aplicación
    //evitando de esta manera hacer variables globales.

})();
