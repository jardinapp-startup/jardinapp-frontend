(function () {
    'use strict';

    angular.module('app.paises.controller', [
    ])
    .controller('paisesCtrl', paisesCtrl);

    paisesCtrl.$inject = ['paisesServices'];

    function paisesCtrl(paisesServices) {
        this.paises = paisesServices.query();
    }

})();
