(function () {
    'use strict';

    angular.module('app.paises.directivas', [

    ]).directive('paises', paises);

    function paises() {
        return {
            scope: {},
            restrict:'EA',
            templateUrl: 'app/paises/lista.html',
            controller: 'paisesCtrl',
            controllerAs: 'vm'
        }
    }

})();
