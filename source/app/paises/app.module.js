(function () {
    'use strict';

    angular.module('app.paises', [
        'app.paises.router',
        'app.paises.directivas',
        'app.paises.controller',
        'app.paises.services'
    ]);

})();
