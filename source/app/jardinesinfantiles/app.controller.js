(function () {
    'use strict';

    angular.module('app.jardinesinfantiles.controller', [
    ])

        .controller('jardinesinfantilesCtrl', jardinesinfantilesCtrl)
        .controller('jardinesinfantilesListAdminCtrl', jardinesinfantilesListAdminCtrl)
        .controller('jardinesinfantilesCreate', jardinesinfantilesCreate)
        .controller('jardinesinfantilesViewCtrl', jardinesinfantilesViewCtrl)
        .controller('jardinesinfantilesUpdateCtrl', jardinesinfantilesUpdateCtrl)
        .controller('jardinesinfantilesMiJardinCtrl', jardinesinfantilesMiJardinCtrl)
        .controller('jardinesinfantilesListaUsuariosVCtrl', jardinesinfantilesListaUsuariosVCtrl);

    jardinesinfantilesMiJardinCtrl.$inject = ['jardinesinfantilesServices', 'SecurityFilter','$location'];
    function jardinesinfantilesMiJardinCtrl(jardinesinfantilesServices, SecurityFilter,$location) {
      if(!SecurityFilter.isAdminJardin()){
        $location.path('/');
      }
      var vm=this;
        jardinesinfantilesServices.getMiJardin().$promise
        .then(function(data){
          vm.jardinesinfantil = data;
        })

        
        this.onChange = function (id) {


          jardinesinfantilesServices.inhabilit({idJardinInfantil:id}).$promise
          .then(function(data){
              console.log("eXITO");
              console.log(data);
          })
          .catch(function(data){
              console.log("NO EXITO");
              console.log(data);
          })
        };

    }

    jardinesinfantilesListAdminCtrl.$inject = ['jardinesinfantilesServices', 'SecurityFilter','$location'];
    function jardinesinfantilesListAdminCtrl(jardinesinfantilesServices, SecurityFilter,$location) {
      if(SecurityFilter.isAdmin()){
        this.jardinesinfantiles = jardinesinfantilesServices.query();
      }else{

        $location.path('/');
      }


        this.onChange = function (id) {


          jardinesinfantilesServices.inhabilit({idJardinInfantil:id}).$promise
          .then(function(data){
              console.log("eXITO");
              console.log(data);
          })
          .catch(function(data){
              console.log("NO EXITO");
              console.log(data);
          })
        };
    }



    jardinesinfantilesListaUsuariosVCtrl.$inject = ['jardinesinfantilesServices','hijosServices','SecurityFilter','$stateParams','$location']
    function jardinesinfantilesListaUsuariosVCtrl(jardinesinfantilesServices,hijosServices,SecurityFilter,$stateParams,$location){
        var vm=this;
      if(!SecurityFilter.isAdminJardin() && !SecurityFilter.isAdmin()){
        $location.path('/');
      }else{
        vm.usuarios =  jardinesinfantilesServices.byCliente({idJardin:$stateParams.idJardinInfantil});
      }

      this.onChange = function (id) {
        hijosServices.inhabilit({idHijo:id}).$promise
        .then(function(data){
            console.log("EXITO");
            console.log(data);
        })
        .catch(function(data){
            console.log("NO EXITO");
            console.log(data);
        })
      };

    }


    jardinesinfantilesCtrl.$inject = ['jardinesinfantilesServices'];
    function jardinesinfantilesCtrl(jardinesinfantilesServices ) {
      var vm=this;
      getJardines();
      function getJardines(){

         jardinesinfantilesServices.public().$promise
        .then(function(data){
        vm.jardinesinfantiles =data;
        });
      }
      vm.cargarTodos=function(){
        vm.criterioBusqueda=null;
        getJardines();
      };
      vm.buscarJardin= function(){
        if(!vm.criterioBusqueda){
          getJardines();
        }else{


        jardinesinfantilesServices.buscarJardin({nombrejardin:vm.criterioBusqueda}).$promise
            .then(function(data){
              vm.jardinesinfantiles =data;
          });
        }
      };
    }

    //Create

    jardinesinfantilesCreate.inject = ['$location', 'SecurityFilter','jardinesinfantilesServices','$mdToast','TipoJardines','$state'];

    function jardinesinfantilesCreate($location,SecurityFilter, jardinesinfantilesServices,$mdToast,TipoJardines,$state) {
      var vm=this;
      vm.mens = '';
      vm.img = false;
      vm.validaEm=false;
      vm.errorValida="";
      if(!SecurityFilter.isAdminJardin()){
        $location.path('/');
      }
      vm.loadImg = loadImg;
      vm.jardinesinfantiles = {};
      vm.queryTipoJardines = jardinesinfantilesServices.queryTipoJardines();
//**************Nuevo Validar que el email no se repita***********
      vm.validaEmail=function(){
        if(vm.jardinesinfantiles.email) {
        jardinesinfantilesServices.findUsuarioByEmail({email:vm.jardinesinfantiles.email}).$promise
        .then(function(data){
            vm.validaEm=false;
        })
        .catch(function(err){
          vm.validaEm=true;
          $mdToast.show(
            $mdToast.simple()
            .textContent('El email ya se encuentra registrado')
            .position('bottom right'));
        });
      } else {
        vm.validaEm=false;
      }
      }

      vm.create = function() {
        if(!vm.validaEm){
        jardinesinfantilesServices.save(vm.jardinesinfantiles).$promise
        .then(function(data){
          $mdToast.show(
            $mdToast.simple()
            .textContent('El jardin infantil fue creado exitosamente')
            .position('bottom right'));
          vm.jardinesinfantiles={};
          $state.go('jardinesinfantiles');
        }).catch(function(){
          $mdToast.show(
            $mdToast.simple()
            .textContent('Error al crear el jardin infantil')
            .position('bottom right'));
        });
      }else{
        vm.errorValida="Ingrese todos los campos en rojo";
        $mdToast.show(
          $mdToast.simple()
          .textContent('Ingrese todos los datos')
          .position('bottom right'));
      }
    }

      function loadImg($fileContent){
        vm.mens = '';
        vm.img = false;
        console.log($fileContent.length);

        if($fileContent.length > 2000){
          console.log('HOLA DE TRUE');
            vm.jardinesinfantiles.foto = $fileContent;
        } else {
          console.log('HOLA DE FALSE');
          vm.mens = 'La imagen es muy grande!';
          vm.img = true;
          vm.jardinesinfantiles.foto = null;
        }
      }

      vm.mensaje = function(){
        return vm.mens;
      }

      vm.imagen = function(){
        return vm.img;
      }

    //Update



      function queryTipoJardines(str) {
        return TipoJardines.queryByNombre({
          query:str
        });

      }
  }

    //Update
    jardinesinfantilesUpdateCtrl.$inject = ['$stateParams', '$location', 'jardinesinfantilesServices','$mdToast','SecurityFilter','$state'];
    function jardinesinfantilesUpdateCtrl($stateParams, $location, jardinesinfantilesServices, $mdToast,SecurityFilter,$state) {
      var vm=this;
      vm.validaEm=false;
      vm.errorValida="";
      if(SecurityFilter.isAdminJardin() || SecurityFilter.isAdmin()){
        vm.jardinesinfantiles = {};
        vm.queryTipoJardines = jardinesinfantilesServices.queryTipoJardines();
        vm.id = $stateParams.idJardinInfantil;
        vm.jardinesinfantiles = jardinesinfantilesServices.get({ idJardinInfantil: vm.id });
        //Con el update se realiza un método PUT
      }else{
        $location.path('/');

      }
      vm.loadImg = function ($fileContent){
        vm.mens = '';
        vm.img = false;
        console.log($fileContent.length);

        if($fileContent.length > 2000){
          console.log('HOLA DE TRUE');
            vm.jardinesinfantiles.foto = $fileContent;
        } else {
          console.log('HOLA DE FALSE');
          vm.mens = 'La imagen es muy grande!';
          vm.img = true;
          vm.jardinesinfantiles.foto = null;
        }
      }

      vm.mensaje = function(){
        return vm.mens;
      }

      vm.imagen = function(){
        return vm.img;
      }

      vm.validaEmail=function(){
        if(vm.jardinesinfantiles.email) {
        jardinesinfantilesServices.findUsuarioByEmail({email:vm.jardinesinfantiles.email}).$promise
        .then(function(data){
            vm.validaEm=false;
        })
        .catch(function(err){
          vm.validaEm=true;
          $mdToast.show(
            $mdToast.simple()
            .textContent('El email ya se encuentra registrado')
            .position('bottom right'));
        });
      } else {
        vm.validaEm=false;
      }
    }
        vm.update = function () {
          if(!vm.validaEm){
            jardinesinfantilesServices.update(vm.jardinesinfantiles).$promise
            .then(function(data){
              $mdToast.show(
                $mdToast.simple()
                .textContent('El jardin infantil fue actualizado exitosamente')
                .position('bottom right'));
              vm.jardinesinfantiles={};
              $state.go('jardinesinfantiles');
            }).catch(function(){
              $mdToast.show(
                $mdToast.simple()
                .textContent('Error al editar el jardin infantil')
                .position('bottom right'));
            });
          }
        }
    }
    jardinesinfantilesViewCtrl.$inject = ['jardinesinfantilesServices', '$location', '$stateParams', 'calificacionesServices', '$mdToast','SecurityFilter']
    function jardinesinfantilesViewCtrl(jardinesinfantilesServices, $location, $stateParams, calificacionesServices, $mdToast,SecurityFilter) {
      var vm=this;
      vm.isAuthenticated=function(){
        return SecurityFilter.isAuthenticated();
      }
        vm.jardinesinfantiles = jardinesinfantilesServices.get({ idJardinInfantil: $stateParams.idJardinInfantil });

        jardinesinfantilesServices.getComentarios({idJardinInfantil:$stateParams.idJardinInfantil}).$promise
        .then(function(data) {
          vm.comentarios=data;
        })
        .catch(function(err){
          console.log(err);
        })

        vm.calificar = function (idJardinInfantil) {
            vm.puntuacion = vm.calificaciones.puntuacion;
            vm.calificaciones = {
                puntuacion: vm.puntuacion,
                idJardinInfantil: {
                    idJardinInfantil: $stateParams.idJardinInfantil
                }
            };
            calificacionesServices.save(vm.calificaciones, function (Data) {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('La calificación fue creada exitosamente')
                        .position('bottom right'));
                console.log(Data);
            });

        };
        vm.newComentario={
          descripcion:"",
          idJardinInfantil:{
            idJardinInfantil:$stateParams.idJardinInfantil
          }
        };
        /*
        {
          "descripcion": "Excelente Jardin Papasssss",
          "idJardinInfantil": {
            "idJardinInfantil": 1
          }
        }
        */
      vm.createComentario=function(){
        //vm.newComentario.descripcion="Excelente servicio en ese jardin"; //esto es para pruebas sin el html
        jardinesinfantilesServices.setComentario(vm.newComentario).$promise
        .then(function(ok){
          console.log(ok);
          vm.newComentario.descripcion="";
          jardinesinfantilesServices.getComentarios({idJardinInfantil:$stateParams.idJardinInfantil}).$promise
          .then(function(data) {
            vm.comentarios=data;
          })
          .catch(function(err){
            console.log(err);
          })
        })
      }
    }

})();
