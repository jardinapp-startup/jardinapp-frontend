(function () {
    'use strict';

    angular.module('app.jardinesinfantiles.services', [

    ])
        .factory('jardinesinfantilesServices', jardinesinfantilesServices)
        .factory('TipoJardines',TipoJardines);



    jardinesinfantilesServices.$inject = ['$resource','BASEURL'];


    //Este servicio nos provee de los métodos GET POST PUT DELETE
    function jardinesinfantilesServices($resource,BASEURL) {
       return $resource(BASEURL+'/jardinesinfantiles/:idJardinInfantil',
       { idJardinInfantil: '@idJardinInfantil' },
       {
         update: {
         method: 'PUT'

       },

         inhabilit : {
             url:BASEURL+'/jardinesinfantiles/inhabilit/:idJardinInfantil',
             method: 'PUT',
             params:{idJardinInfantil: '@idJardinInfantil'}
         },

         public : {
             url:BASEURL+'/jardinesinfantiles/public',
             method: 'GET',
             isArray:true

         },
         getMiJardin:{
           url:BASEURL+'/jardinesinfantiles/mijardin/:idJardinInfantil',
           isArray:true,
           method: 'GET'
         },

         byCliente:{
           url:BASEURL+'/jardinesinfantiles/bycliente/:idJardin',
           method: 'GET',
           isArray:true,
           params :{idJardin :'@idJardin'}
         },
         queryTipoJardines:{
           url:BASEURL + '/tiposjardines',
           method:'GET',
           isArray:true
         },

         getComentarios: {
           url: BASEURL+'/comentarios/byJardin/:idJardinInfantil',
           method: 'GET',
           isArray: true,
           params:{
             idJardinInfantil:'@idJardinInfantil'
           }
         },
         setComentario:{
           url:BASEURL+'/comentarios/setComentario/',
           method:'POST'
         },
         findUsuarioByEmail:{
           url:BASEURL+'/jardinesinfantiles/findUsuarioByEmail/:email',
           method:'GET',
           params:{
             email:'@email'
           }
         },
         buscarJardin:{
           url: BASEURL+'/jardinesinfantiles/search/:nombrejardin',
           method: 'GET',
           isArray: true,
           params:{
             nombrejardin:'@nombrejardin'
           }
         }

       })
    }

TipoJardines.$inject = ['$resource','BASEURL'];

function TipoJardines($resource, BASEURL) {
  return $resource(BASEURL + '/tiposjardines/:idTipoJardines',{
    idTipoJardines:'@idTipoJardines'
  },{
    queryByNombre:{
      url:BASEURL + '/tiposjardines/nombre/:query',
      method:'GET',
      isArray:true,
      params:{
        query:'@query'
      }
    }
  });
}


})();
