(function () {
    'use strict';

    angular.module('app.jardinesinfantiles', [
        'app.jardinesinfantiles.router',
        'app.jardinesinfantiles.directivas',
        'app.jardinesinfantiles.controller',
        'app.jardinesinfantiles.services'
    ]);

})();
