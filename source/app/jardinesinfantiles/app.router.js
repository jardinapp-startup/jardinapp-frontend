(function () {
    'use strict';

    angular.module('app.jardinesinfantiles.router', [

    ])
        .config(configure);

    //Se inyecta los parametros
    configure.$inject = ['$stateProvider', '$urlRouterProvider'];

    //Se configura las rutas de la aplicación para modelo
    function configure($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');
 
        $stateProvider
            .state('jardinesinfantiles', {
                url: '/jardinesinfantiles',
                template: '<jardinesinfantiles/>'
            })
            .state('jardinesinfantileslistadmin', {
                url: '/jardinesinfantiles/listadmin',
                template: '<jardinesinfantileslistadmin/>'
            })
             .state('jardinesinfantileslistusuariosv', {
              url: '/jardinesinfantiles/listusuariosv/:idJardinInfantil',
                template: '<jardinesinfantileslistusuariosv/>'

            })
            .state('jardinesinfantilescreate', {
                url: '/jardinesinfantiles/create',
                template: '<jardinesinfantilescreate/>'
            })
            .state('jardinesinfantilesupdate', {
                url: '/jardinesinfantiles/update/:idJardinInfantil',
                template: '<jardinesinfantilesupdate/>'
            })
            .state('jardinesinfantilesview', {
                url: '/jardinesinfantiles/view/:idJardinInfantil',
                template: '<jardinesinfantilesview/>'
            })
            .state('jardinesinfantilesmijardin', {
                url: '/jardinesinfantiles/mijardin',
                template: '<mijardininfantil/>'
            })
      };
})();
