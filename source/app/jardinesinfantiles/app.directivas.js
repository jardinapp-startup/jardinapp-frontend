(function () {
    'use strict';

    angular.module('app.jardinesinfantiles.directivas', [

    ]).directive('jardinesinfantiles', jardinesinfantiles)
     .directive('jardinesinfantileslistadmin', jardinesinfantilesListAdmin)
    .directive('jardinesinfantilescreate', jardinesinfantilesCreate)
    .directive('jardinesinfantilesupdate', jardinesinfantilesUpdate)
    .directive('jardinesinfantilesview', jardinesinfantilesView)

    .directive('jardinesinfantileslistusuariosv', jardinesinfantilesListaUsuariosV)

    .directive('mijardininfantil', miJardinInfantil)

    function miJardinInfantil() {
      console.log("mi jardin");
        return {
            scope: {},
            restrict:'EA',
            templateUrl: 'app/jardinesinfantiles/mijardin.html',
            controller:  'jardinesinfantilesMiJardinCtrl',
            controllerAs: 'vm'
        }
    }
    function jardinesinfantiles() {
        return {
            scope: {},
            restrict:'EA',
            templateUrl: 'app/jardinesinfantiles/lista.html',
            controller:  'jardinesinfantilesCtrl',
            controllerAs: 'vm'
        }
    }

      function jardinesinfantilesListAdmin() {
        return {
            scope: {},
            restrict:'EA',
            templateUrl: 'app/jardinesinfantiles/listaAdmin.html',
            controller:  'jardinesinfantilesListAdminCtrl',
            controllerAs: 'vm'
        }
    }

     function jardinesinfantilesListaUsuariosV(){

      return {
          scope: {},
          restrict:'EA',
          templateUrl: 'app/jardinesinfantiles/listaUsuariosV.html',
          controller:  'jardinesinfantilesListaUsuariosVCtrl', 
          controllerAs: 'vm'
      }
    }

    function jardinesinfantilesCreate() {
      return {
          scope: {},
          restrict:'EA',
          templateUrl: 'app/jardinesinfantiles/create.html',
          controller:  'jardinesinfantilesCreate',
          controllerAs: 'vm'
      }
    }

    function jardinesinfantilesUpdate() {
      return {
          scope: {},
          restrict:'EA',
          templateUrl: 'app/jardinesinfantiles/update.html',
          controller:  'jardinesinfantilesUpdateCtrl',
          controllerAs: 'vm'
      }
    }
    function jardinesinfantilesView(){
      return {
          scope: {},
          restrict:'EA',
          templateUrl: 'app/jardinesinfantiles/jardin_view.html',
          controller:  'jardinesinfantilesViewCtrl',
          controllerAs: 'vm'
      }
    }
})();
