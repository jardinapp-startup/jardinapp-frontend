(function () {
    'use strict';

    angular.module('app.securityFilter', [
      'satellizer'
    ])
        .factory('SecurityFilter', SecurityFilter)
        SecurityFilter.$inject=['$auth','$mdToast','$location']
      function  SecurityFilter ($auth,$mdToast,$location){
        var Filter =  {
            isAuthenticated:function(){
                if($auth.isAuthenticated()){
                  return true;
                }else{
                  return false;
                }
            },
            isAdmin:function(){
              if(Filter.isAuthenticated()){
                return $auth.getPayload().roles.indexOf('adminP') !== -1; // -1 si ADMIN no esta en el arreglo y retorna false
              }else{
                return false;
              }
            },
            isAdminJardin:function(){
                if(Filter.isAuthenticated()){
                  return $auth.getPayload().roles.indexOf('adminJ') !== -1; // -1 si ADMIN no esta en el arreglo y retorna false
                }else{
                  return false;
                }
              },
            isEmpresario:function (){
              if(Filter.isAuthenticated()){
                return $auth.getPayload().roles.indexOf('empresario') !== -1; // -1 si es empresario no esta en el arreglo y retorna false
              }else{
                return false;
              }
            },
            isUser:function (){
                if(Filter.isAuthenticated()){
                  return $auth.getPayload().roles.indexOf('usuario') !== -1; // -1 si usuario no esta en el arreglo y retorna false
                }else{
                  return false;
                }
              },
             getCurrentUser:function(){
                if(Filter.isAuthenticated()){
                  return $auth.getPayload().user;
                }else{
                  return;
                }
              },
             getCurrentUserId:function (){
                if(Filter.isAuthenticated()){
                  return $auth.getPayload().sub;
                }else{
                  return null;
                }
              },
              isEstado:function(){
                  return $auth.isEstado();
              },
              signIn:function(dataLogin){
                 $auth.login(dataLogin)
                  .then(function(){
                    $mdToast.show(
                      $mdToast.simple()
                      .textContent('Inicio Sesión Correctamente')
                      .position('bottom right'));
                    $location.path('/jardinesinfantiles');

                  })
                  .catch(function(err){
                    console.log(err.status+' '+err.data);
                    $mdToast.show(
                      $mdToast.simple()
                      .textContent('Email o password incorrecto')
                      .position('bottom right'));
                  });
              },
              logOut:function(){
                if(Filter.isAuthenticated()){
                    $auth.logout()
                    .then(function(){
                      console.log('Salida segura..');
                    $location.path('/');
                  });

                  }else{
                    return;
                  }
              }


          };

          return Filter;

      }//funcion
})();
