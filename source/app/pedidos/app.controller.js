(function () {
    'use strict';

    angular.module('app.pedidos.controller', [
    ])
    .controller('pedidosCtrl', pedidosCtrl)
    .controller('pedidosCreate', pedidosCreate)
    .controller('pedidosUpdate', pedidosUpdate);


    pedidosCtrl.$inject = ['pedidosServices', 'SecurityFilter'];

    function pedidosCtrl(pedidosServices, SecurityFilter) {
        this.pedidos = pedidosServices.query();
        this.isAdmin=isAdmin;

        function isAdmin(){
            //*********  PRUEBA IS ADIMIN ******
       return SecurityFilter.isAdmin();
      }
    }

    //Create
      pedidosCreate.$inject = ['$location', 'pedidosServices','$mdToast'];

    function pedidosCreate($location, pedidosServices,$mdToast){
      this.create = function() {
        pedidosServices.save(this.pedidos, function(){
          $mdToast.show(
            $mdToast.simple()
            .textContent('El pedido fue creado exitosamente')
            .position('bottom right'));
          $location.path('/pedidos');
        });
      }
    }

    //Update
    pedidosUpdate.$inject = ['$stateParams', '$location', 'pedidosServices', '$mdToast'];

    function pedidosUpdate($stateParams, $location, pedidosServices, $mdToast) {
        this.id = $stateParams.idPedido;
        this.pedidos = pedidosServices.get({ idPedido: this.id });
        console.log(this.pedidos);
        //Con el update se realiza un método PUT
        this.update = function() {
            pedidosServices.update(this.pedidos, function() {
              $mdToast.show(
                $mdToast.simple()
                .textContent('El pedido fue actualizado exitosamente')
                .position('bottom right'));
                $location.path('/pedidos');
            });
        }
    }
})();
