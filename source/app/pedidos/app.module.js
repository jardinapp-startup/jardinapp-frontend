(function () {
    'use strict';

    angular.module('app.pedidos', [
        'app.pedidos.router',
        'app.pedidos.directivas',
        'app.pedidos.controller',
        'app.pedidos.services'
    ]);

})();
