
(function () {
    'use strict';

    angular.module('app.pedidos.router', [

    ])
        .config(configure);


    configure.$inject = ['$stateProvider', '$urlRouterProvider'];

    function configure($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('pedidos', {
                url: '/pedidos',
                template: '<pedidos/>'
            })
            .state('pedidoscreate',{
              url: '/pedidos/create',
               template: '<pedidoscreate/>'
            })
            .state('pedidosupdate',{
              url: '/pedidos/update/:idPedido',
             template: '<pedidosupdate/>'
            })
    };
})();
