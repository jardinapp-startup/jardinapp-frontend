(function () {
    'use strict';

    angular.module('app.pedidos.directivas', [

    ]).directive('pedidos', pedidos)
    .directive('pedidoscreate', pedidosCreate)
    .directive('pedidosupdate', pedidosUpdate);


    function pedidos() {
        return {
            scope: {},
            restrict:'EA',
            templateUrl: 'app/pedidos/lista.html',
            controller: 'pedidosCtrl',
            controllerAs: 'vm'
        }
    }

    function pedidosCreate(){
      return {
        scope: {},
        restrict:'EA',
        templateUrl: 'app/pedidos/create.html',
        controller: 'pedidosCreate',
        controllerAs: 'vm'
      }
    }

    function pedidosUpdate(){
      return {
        scope: {},
        restrict:'EA',
        templateUrl: 'app/pedidos/update.html',
        controller: 'pedidosUpdate',
        controllerAs: 'vm'
      }
    }

})();
