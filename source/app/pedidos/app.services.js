(function () {
    'use strict';

    angular.module('app.pedidos.services', [

    ])
        .factory('pedidosServices', pedidosServices);

    pedidosServices.$inject = ['$resource','BASEURL'];

    function pedidosServices($resource,BASEURL) {
       return $resource(BASEURL+'/pedidos/:idPedido',
       { idPedido: '@idPedido' },
       { 'update': {method: 'PUT'}
     });
    }
})();
