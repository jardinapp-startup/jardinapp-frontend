(function () {
  'use strict';

  angular.module('app.usuarios.controller', [
    'app.usuarios.router',
    'ngMaterial'

  ])
  .controller('usuariosCtrl', usuariosCtrl)
  .controller('usuariosUpdateCtrl', usuariosUpdateCtrl)
  .controller('updatePasswordCtrl', updatePasswordCtrl)
  .controller('deshabilitarCuentaCtrl', deshabilitarCuentaCtrl)
  .controller('UpdatepassCtrl',UpdatepassCtrl)
  .controller('usuariosPerfilCtrl', usuariosPerfilCtrl)
  usuariosUpdateCtrl.$inject = ['usuariosServices','$location','$stateParams','$state','SecurityFilter'];
  usuariosPerfilCtrl.$inject = ['usuariosServices','$location','$stateParams','$state', 'Ciudades', 'tiposDocumentos','$mdDialog','$scope','SecurityFilter','$auth'];

  usuariosCtrl.$inject = ['$auth','usuariosServices','$location','$stateParams','$state','Ciudades','tiposDocumentos','$mdToast','SecurityFilter'];
  updatePasswordCtrl.$inject = ['usuariosServices','$location','$stateParams','$state','SecurityFilter'];
  function usuariosCtrl($auth,usuariosServices,$location,$stateParams,$state,Ciudades,tiposDocumentos,$mdToast,SecurityFilter){
    var vm=this;
      vm.validaDoc=false;
      vm.validaEm=false;
      vm.errorValida="";
      vm.mens = '';
      vm.img = false;
      vm.queryCiudades = queryCiudades;
      vm.queryTipoDocumento = tiposDocumentos.getTipoDocumentos();
      if(SecurityFilter.isAdmin()){

      usuariosServices.query().$promise.
      then(function(data){
        vm.usuarios = data;
      });
    }
    vm.loadAvatar=function($fileContent){
      vm.mens = '';
      vm.img = false;
      console.log($fileContent.length);

      if($fileContent.length > 2000){
        console.log('HOLA DE TRUE');
          vm.usuario.avatar=$fileContent;;
      } else {
        console.log('HOLA DE FALSE');
        vm.mens = 'La imagen es muy grande!';
        vm.img = true;
        vm.usuario.avatar = null;
      }
    }

    vm.mensaje = function(){
      return vm.mens;
    }

    vm.imagen = function(){
      return vm.img;
    }
      //vm.usuario.avatar=$fileContent;

      vm.onChange = function (id) {
          usuariosServices.inhabilit({idUsuarios:id}).$promise
          .then(function(data){
          })
          .catch(function(data){
          });
      };
      vm.validaDocumento = function(){
        if (vm.usuario.documento) {
          usuariosServices.findUsuarioByDocumento({documento:vm.usuario.documento}).$promise
          .then(function(data){

            vm.validaDoc=false;
          })
          .catch(function(err){
            vm.validaDoc=true;
            $mdToast.show(
              $mdToast.simple()
              .textContent('El número de ducumento ya se encuentra registrado')
              .position('bottom right'));
          });
        }
          else {
              vm.validaDoc=false;
          }

      }
      vm.validaEmail=function(){
        if (vm.usuario.email) {
        usuariosServices.findUsuarioByEmail({email:vm.usuario.email}).$promise
        .then(function(data){
            vm.validaEm=false;
        })
        .catch(function(err){
          vm.validaEm=true;
          $mdToast.show(
            $mdToast.simple()
            .textContent('El email ya se encuentra registrado')
            .position('bottom right'));
        });
      } else {
        vm.validaEm=false;
      }
      }

      vm.create=function(){

        if(!vm.validaDoc && !vm.validaEm){
        usuariosServices.save(vm.usuario).$promise
        .then(function(data){

          $mdToast.show(
            $mdToast.simple()
            .textContent('El usuario fue creado exitosamente')
            .position('bottom right'));
          vm.usuario={};
          $state.go('login');
        }).catch(function(){
          $mdToast.show(
            $mdToast.simple()
            .textContent('Error al crear el usuario')
            .position('bottom right'));


        });


      }else{
        vm.errorValida="Ingrese todos los campos en rojo";
        $mdToast.show(
          $mdToast.simple()
          .textContent('Ingrese todos los datos')
          .position('bottom right'));
      }
    }
      vm.createJardin=function(){
        if(!vm.validaDoc && !vm.validaEm){

        usuariosServices.createJardin(vm.usuario).$promise
        .then(function(data){
          $mdToast.show(
            $mdToast.simple()
            .textContent('El jardin fue creado exitosamente')
            .position('bottom right'));
          vm.usuario={};
          $state.go('login');
        }).catch(function(){
          $mdToast.show(
            $mdToast.simple()
            .textContent('Error al crear el jardin')
            .position('bottom right'));
        })
      }else{
          vm.errorValida="Ingrese todos los campos en rojo";
              $mdToast.show(
                $mdToast.simple()
                .textContent('Ingrese todos los datos')
                .position('bottom right'));
            }
      }
        vm.createEmpresa=function(){
          if(!vm.validaDoc && !vm.validaEm){
        usuariosServices.createEmpresa(vm.usuario).$promise
        .then(function(data){
          $mdToast.show(
            $mdToast.simple()
            .textContent('La cuenta para empresa fue creada exitosamente')
            .position('bottom right'));
          $state.go('login');
          vm.usuario={};
        }).catch(function(){
          $mdToast.show(
            $mdToast.simple()
            .textContent('Error al crear la cuenta para empresa')
            .position('bottom right'));
        })
      }else{
          vm.errorValida="Ingrese todos los campos en rojo";
              $mdToast.show(
                $mdToast.simple()
                .textContent('Ingrese todos los datos')
                .position('bottom right'));
            }
      }

      function queryCiudades(str){
        return Ciudades.queryByNombre({
          query: str
        });
      }




      vm.update=function(){
        //delete vm.usuario.rolesList;
        console.log(vm.usuario);
        usuariosServices.update({idUsuarios:vm.usuario.idUsuarios},vm.usuario);
        vm.usuario={};
        $state.go('usuarios');

      }

  }
  function usuariosUpdateCtrl(usuariosServices,$location,$stateParams,$state,SecurityFilter){
    var vm=this;
    //vm.usuario={};
    if(!SecurityFilter.isAuthenticated()){
      $location.path('/');
    }else{
      vm.usuario={};
      vm.usuario = usuariosServices.get({idUsuarios:$stateParams.idUsuarios});
    }
      vm.update=function(){
        usuariosServices.update({idUsuarios:vm.usuario.idUsuarios},vm.usuario);

        vm.usuario={};
        $state.go('usuarios');

      }
    vm.loadAvatar=function($fileContent){
      vm.usuario.avatar=$fileContent;
    }

  }//Fin update Controller

  function usuariosPerfilCtrl(usuariosServices,$location,$stateParams,$state,Ciudades,tiposDocumentos, $mdDialog,$scope,SecurityFilter,$auth){
    var vm=this;
    vm.mens = '';
    vm.img = false;
    if(!SecurityFilter.isAuthenticated()){
      $location.path('/');
    }else{
      vm.queryCiudades = queryCiudades;
       vm.usuario={};
      vm.queryTiposDocumentos = queryTiposDocumentos;
      vm.usuario = usuariosServices.get({idUsuarios:SecurityFilter.getCurrentUserId()});
      console.log('test Read usuarios');
    }

      vm.update=function(){

        usuariosServices.update(vm.usuario,function(){
          console.log('Update Perfil Ok');
          $location.path('/usuarios/perfil');

        });

      };

      vm.loadAvatar=function($fileContent){
        vm.mens = '';
        vm.img = false;
        console.log($fileContent.length);

        if($fileContent.length > 2000){
          console.log('HOLA DE TRUE');
            vm.usuario.avatar=$fileContent;;
        } else {
          console.log('HOLA DE FALSE');
          vm.mens = 'La imagen es muy grande!';
          vm.img = true;
          vm.usuario.avatar = null;
        }
      }

      vm.mensaje = function(){
        return vm.mens;
      }

      vm.imagen = function(){
        return vm.img;
      }
        function queryCiudades(str){
          return Ciudades.queryByNombre({
            query: str
          });
        }

        function queryTiposDocumentos(str){
          return tiposDocumentos.queryByNombre({
            query: str
          });
        }
      }// fin controller

      //Inicio actualizar contrseña
      UpdatepassCtrl.$inject = ['usuariosServices', '$location', '$mdToast', 'SecurityFilter', '$state'];

      function UpdatepassCtrl(usuariosServices, $location, $mdToast, SecurityFilter, $state) {
          var vm = this;
          // vm.confirmar = {};
          if (SecurityFilter.isAuthenticated()) {
              vm.updatepass = function() {
                  //vm.usuario.password = vm.passwordnew;
                  usuariosServices.updatePassword({
                      id: SecurityFilter.getCurrentUserId(),
                      passOld: vm.passwordNow,
                      passNew: vm.passwordNew
                  }).$promise.then(function(data){
                    console.log('OK');
                    console.log(data);
                    $location.path('/usuarios/perfil');
                    $mdToast.show(
                        $mdToast.simple()
                        .textContent('Se ha  actualizado la contraseña correctamente...')
                        .position('bottom right'));
                  }).catch(function(err){
                    console.log('ERROR');
                    console.log(err);
                    $location.path('/usuarios/perfil');
                    $mdToast.show(
                        $mdToast.simple()
                        .textContent('Error al actualizar, las contraseña actual no coincide...' + err)
                        .position('bottom right'));
                  });

              };
          } else {
              $location.path('/');
          }
      }
      //Fin actualizar contraseña
      deshabilitarCuentaCtrl.$inject = ['$auth', 'usuariosServices', '$location', '$stateParams' , '$mdToast','SecurityFilter'];
      function deshabilitarCuentaCtrl($auth,usuariosServices,$location,$stateParams,$mdToast,SecurityFilter) {
        var vm = this;
        if(!SecurityFilter.isAuthenticated()){
          $location.path('/');
        }else{

        vm.getCurrentUser = SecurityFilter.getCurrentUser;
        vm.idUsuario = $stateParams.idUsuario;
        vm.logout=logout;
      }

        function logout(){
          if(SecurityFilter.isAuthenticated()){
            $auth.logout()
            .then(function(){
            $location.path('/');
          });

          }else{
            return;
          }
        }
          vm.deshabilitar = function() {
              usuariosServices.inhabilit({idUsuarios:SecurityFilter.getCurrentUserId()}).$promise
              .then(function(data){
                $mdToast.show(
                  $mdToast.simple()
                  .textContent('Gracias por todo')
                  .position('bottom right'));
                  logout();
                  $location.path("#/inicio");
              })
          }



      }

      function updatePasswordCtrl(usuariosServices,$location,$stateParams,$state,SecurityFilter){
        var vm=this;
        if(!SecurityFilter.isAuthenticated()){
          $location.path('/');
        }else{

          vm.usuario = usuariosServices.get({idUsuarios:SecurityFilter.getCurrentUserId()});
        }
          vm.update=function(){
            vm.usuario.password=vm.passwordnew;
            usuariosServices.updatePassword({idUsuarios:vm.usuario.idUsuarios},vm.usuario);
            vm.usuario={};
              $location.path('/usuarios/perfil');

          }
      }
})();
