(function () {
    'use strict';

    angular.module('app.usuarios', [
        'app.usuarios.directivas',
        'app.usuarios.controller',
        'app.usuarios.services',
        'app.usuarios.router'
    ]);

})();
