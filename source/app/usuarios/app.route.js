(function () {
    'use strict';

    angular.module('app.usuarios.router', [
        'app.usuarios.services',
        'app.usuarios.controller'
    ])
        .config(configure);

    //Se inyecta los parametros
    configure.$inject = ['$stateProvider', '$urlRouterProvider'];

    //Se configura las rutas de la aplicación para modelo
    function configure($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/usuarios');

        $stateProvider
            .state('usuarios', {
                url: '/usuarios',
                template: '<usuarios/>'
            })
            .state('create',{
              url:'/usuarios/cuentas/padres',
              template: '<padres-create/>'
            })
            .state('update',{
              url:'/usuarios/update/:idUsuarios',
              template: '<updateusuario/>'
            })

            .state('perfilpassword',{
                url:'/usuarios/perfil/update/password',
                template: '<update-password/>'
            })
            .state('jardinescreate',{
                url:'/usuarios/cuentas/jardines',
                template: '<jardines-create/>'
            })
            .state('empresascreate',{
                url:'/usuarios/cuentas/empresas',
                template: '<empresas-create/>'
            })
            .state('deshabilitarcuenta',{
                url:'/usuarios/perfil/deshabilitarcuenta',
                template: '<deshabilitarcuenta/>'
            })
            .state('perfil',{
                url:'/usuarios/perfil',
                template: '<perfil/>'
            })
            .state('perfilupdate',{
                url:'/usuarios/perfil/update',
                template: '<updateperfil/>'
            })

    };


})();
