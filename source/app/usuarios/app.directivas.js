(function() {
    'use strict';

    angular.module('app.usuarios.directivas', [

    ]).directive('usuarios', usuarios)
    .directive('padresCreate',padresCreate)
    .directive('updateusuario',update)
    .directive('perfil',perfil)
    .directive('updateperfil',updatePerfil)
    .directive('jardinesCreate',jardinesCreate)
    .directive('empresasCreate',empresasCreate)
    .directive('updatePassword',updatePassword)
    .directive('deshabilitarcuenta',deshabilitarCuenta)
    function usuarios() {
        return {
            scope: {},
            restrict:'EA',
            templateUrl: 'app/usuarios/lista.html',
            controller: 'usuariosCtrl',
            controllerAs: 'vm'
        }
    }
    function padresCreate(){
      return{
        scope: {},
        restrict:'EA',
        templateUrl: 'app/usuarios/padres_create.html',
        controller: 'usuariosCtrl',
        controllerAs: 'vm'

      }
    }

    function update(){
      return{
        scope: {},
        restrict:'EA',
        templateUrl: 'app/usuarios/update.html',
        controller: 'usuariosUpdateCtrl',
        controllerAs: 'vm'

      }
    }

    function updatePassword() {
        return {
            scope: {},
            restrict:'EA',
            templateUrl: 'app/usuarios/update_pass.html',
            controller: 'UpdatepassCtrl',
            controllerAs: 'vm'
        }
    }

    function jardinesCreate() {
        return {
            scope: {},
            restrict:'EA',
            templateUrl: 'app/usuarios/jardines_create.html',
            controller: 'usuariosCtrl',
            controllerAs: 'vm'
        }
    }

    function empresasCreate() {
        return {
            scope: {},
            restrict:'EA',
            templateUrl: 'app/usuarios/empresas_create.html',
            controller: 'usuariosCtrl',
            controllerAs: 'vm'
        }
    }

    function deshabilitarCuenta() {
      return {
        scope: {},
        restrict:'EA',
        templateUrl: 'app/usuarios/inhabilitar.html',
        controller: 'deshabilitarCuentaCtrl',
        controllerAs: 'vm'
      }
    }
    function updatePerfil(){
      return{
        scope: {},
        restrict:'EA',
        templateUrl: 'app/usuarios/update.html',
        controller: 'usuariosPerfilCtrl',
        controllerAs: 'vm'

      }
    }
    function perfil(){
      return{
        scope: {},
        restrict:'EA',
        templateUrl: 'app/usuarios/perfil.html',
        controller: 'usuariosPerfilCtrl',
        controllerAs: 'vm'
      }
    }
})();
