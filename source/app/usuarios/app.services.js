(function () {
    'use strict';

    angular.module('app.usuarios.services', [

    ])
        .factory('usuariosServices', usuariosServices)
        .factory('Ciudades', Ciudades)
        .factory('tiposDocumentos', tiposDocumentos);

    usuariosServices.$inject = ['$resource','BASEURL'];


    //Este servicio nos provee de los métodos GET POST PUT DELETE
    function usuariosServices($resource,BASEURL) {
       return $resource(BASEURL+'/usuarios/:idUsuarios',{idUsuarios:'@idUsuarios'},
       {
         update: {  method: 'PUT'},
         updatePassword:{
           url:BASEURL+'/usuarios/updatepass/:idUsuarios',
           method:'PUT'
         },
         updatePassword: {
           url: BASEURL + '/usuarios/updatepassw/:id/:passOld/:passNew',
           method: 'PUT',
           params: {
             id: '@id',
             passOld: '@passOld',
             passNew: '@passNew'
           }
         },
         inhabilit: {
           url: BASEURL+'/usuarios/inhabilit/:idUsuarios',
           method: 'PUT',
           params: {idUsuarios: '@idUsuarios'}
         },
         createJardin:{
          url:BASEURL+'/usuarios/createjardin/',
          method:'POST'
         },
         createEmpresa:{
          url:BASEURL+'/usuarios/createempresario/',
          method:'POST'
        },
        findUsuarioByDocumento:{
          url:BASEURL+'/usuarios/findUsuarioByDocumento/:documento',
          method:'GET',
          params:{
            documento:'@documento'
          }
        },
        findUsuarioByDocumentoHijos:{
          url:BASEURL+'/usuarios/findUsuarioByDocumentoHijos/:documento',
          method:'GET',
          params:{
            documento:'@documento'
          }
        },
        findUsuarioByEmail:{
          url:BASEURL+'/usuarios/findUsuarioByEmail/:email',
          method:'GET',
          params:{
            email:'@email'
          }
        }
       });


    }

    Ciudades.$inject = ['$resource', 'BASEURL'];

    function Ciudades($resource, BASEURL) {
      return $resource(BASEURL + '/ciudades/:idCiudad', {
        idCiudad: '@idCiudad'
      },{
        queryByNombre: {
          url: BASEURL + '/ciudades/nombre/:query',
          method: 'GET',
          isArray: true,
          params: {
            query: '@query'
          }
        }
      })
    }


    tiposDocumentos.$inject = ['$resource', 'BASEURL'];

    function tiposDocumentos($resource, BASEURL) {
      return $resource(BASEURL + '/tiposdocumentos/:idTipoDocumento', {
        idTipoDocumento: '@idTipoDocumento'
      },{
        getTipoDocumentos: {
          url: BASEURL + '/tiposdocumentos',
          method: 'GET',
          isArray: true
        }
      })
    }
    //De igual manera los factory nos sirven para almacenar información
    //y que nos pueda servir en cualquier controlador o lugar de la aplicación
    //evitando de esta manera hacer variables globales.

})();
