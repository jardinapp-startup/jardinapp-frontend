(function () {
    'use strict';

    angular.module('app.servicios.services', [

    ]).factory('serviciosServices', serviciosServices);

    serviciosServices.$inject = ['$resource','BASEURL'];

    function serviciosServices($resource,BASEURL) {
       return $resource(BASEURL+'/servicios/:idServicio',
       { idServicio: '@idServicio' },
       { 'update': {method: 'PUT'}
     });
    }

})();
