(function () {
    'use strict';

    angular.module('app.servicios.router', [
    ])
        .config(configure);

    //Se inyecta los parametros
    configure.$inject = ['$stateProvider', '$urlRouterProvider'];

    //Se configura las rutas de la aplicación para modelo
    function configure($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('servicios', {
                url: '/servicios',
                template: '<servicios/>'
            })
            .state('servicioscreate', {
                url: '/servicios/create',
                template: '<servicioscreate/>'
            })
            .state('serviciosupdate', {
                url: '/servicios/update/:idServicio',
                template: '<serviciosupdate/>'
            });
    }
})();
