(function () {
    'use strict';

    angular.module('app.servicios', [
        'app.servicios.router',
        'app.servicios.directivas',
        'app.servicios.controller',
        'app.servicios.services'
    ]);

})();
