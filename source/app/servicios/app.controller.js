(function () {
    'use strict';

    angular.module('app.servicios.controller', [
    ])
    .controller('serviciosCtrl', serviciosCtrl)
    .controller('serviciosCreateCtrl', serviciosCreateCtrl)
    .controller('serviciosUpdateCtrl', serviciosUpdateCtrl);

    //List
    serviciosCtrl.$inject = ['serviciosServices','$location','SecurityFilter','$mdDialog','$scope'];
    function serviciosCtrl(serviciosServices,$location,SecurityFilter,$mdDialog,$scope) {
      var vm=this;
        vm.isAuthenticatedAndAdmin=false;

        if(isAdmin()===true){
          vm.isAuthenticatedAndAdmin=true;
        }
        function isAdmin(){
          return SecurityFilter.isAdmin();
        }

        vm.servicios = serviciosServices.query();
        
        vm.verificarLogin = function(idServicio){
          if(SecurityFilter.isAuthenticated()){
              $location.path('/detallespedidos/create/' + idServicio);
          }else{
                $mdDialog.show({
      controller: DialogController,
      controllerAs:'vm',
      templateUrl: 'app/servicios/modal.html',
      parent: angular.element(document.body),
      
      clickOutsideToClose:true
    })
        .then(function(answer) {
          $scope.status = 'You said the information was "' + answer + '".';
        }, function() {
          $scope.status = 'You cancelled the dialog.';
        });
              console.log('debes logearte'); 
          }  
        };

    }

    //Create
    serviciosCreateCtrl.$inject = ['$location', 'serviciosServices','SecurityFilter'];
    function serviciosCreateCtrl($location, serviciosServices,SecurityFilter) {
      if(!SecurityFilter.isAdmin()){
        $location.path('/');
      }

      this.create = function() {
        serviciosServices.save(this.servicios, function(){
          $location.path('/servicios');
        });
      }
    }

    //Update
    serviciosUpdateCtrl.$inject = ['$stateParams', '$location', 'serviciosServices','SecurityFilter'];
    function serviciosUpdateCtrl($stateParams, $location, serviciosServices,SecurityFilter) {
      if(!SecurityFilter.isAdmin()){
        $location.path('/');
      }
      this.id = $stateParams.idServicio;
      this.servicios = serviciosServices.get({ idServicio: this.id });
      console.log(this.servicios);
      //Con el update se realiza un método PUT
      this.update = function() {
          serviciosServices.update(this.servicios, function() {
              $location.path('/servicios');
          });
      }
  }

    DialogController.$inject = ['$scope','$mdDialog','$location'];
    function DialogController($scope, $mdDialog, $location) {
        var vm= this;
        vm.login = function(){
           $location.path('/login'); 
            $mdDialog.hide();
        };
        
         vm.registro = function(){
             $mdDialog.hide();
           $location.path('/inicio/cuentas'); 
        };
        
  $scope.hide = function() {
    $mdDialog.hide();
     
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.answer = function(answer) {
    $mdDialog.hide(answer);
  };
} 
})();
