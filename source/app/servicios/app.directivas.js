(function () {
    'use strict';

    angular.module('app.servicios.directivas', [

    ]).directive('servicios', servicios)
      .directive('servicioscreate', serviciosCreate)
      .directive('serviciosupdate', serviciosUpdate);

    function servicios() {
        return {
            scope: {},
            restrict:'EA',
            templateUrl: 'app/servicios/lista.html',
            controller: 'serviciosCtrl',
            controllerAs: 'vm'
        }
      }

      function serviciosCreate() {
          return {
              scope: {},
              restrict:'EA',
              templateUrl: 'app/servicios/create.html',
              controller: 'serviciosCreateCtrl',
              controllerAs: 'vm'
        }
      }

      function serviciosUpdate() {
          return {
              scope: {},
              restrict:'EA',
              templateUrl: 'app/servicios/update.html',
              controller: 'serviciosUpdateCtrl',
              controllerAs: 'vm'
        }
      }

})();
