
(function () {
    'use strict';

    angular.module('app.respuestasInquietudes.router', [

    ])
        .config(configure);

    //Se inyecta los parametros
    configure.$inject = ['$stateProvider', '$urlRouterProvider'];

    //Se configura las rutas de la aplicación para modelo
    function configure($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('respuestasinquietudes', {
                url: '/respuestasinquietudes/responder/:id',
                template: '<respuestas-inquietudes-create/>'
            })

             .state('respuestasinquietudesview', {
                url: '/respuestasinquietudes/view/:id',
                template: '<respuestas-inquietudes-view/>'
            })
            ;
    };
})();
