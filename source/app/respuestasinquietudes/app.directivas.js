
(function () {
    'use strict';

    angular.module('app.respuestasInquietudes.directivas', [

    ])
    .directive('respuestasInquietudes', respuestasInquietudes)
    .directive('respuestasInquietudesCreate', respuestasInquietudesCreate)

    ;

    function respuestasInquietudes() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/respuestasinquietudes/lista.html',
            controller: 'respuestasInquietudesCtrl',
            controllerAs: 'vm'
        }
    }
function respuestasInquietudesCreate() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/respuestasinquietudes/create.html',
            controller: 'respuestasInquietudesCreateCtrl',
            controllerAs: 'vm'
        }
    }


})();
