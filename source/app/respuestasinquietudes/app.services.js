
(function () {
    'use strict';

    angular.module('app.respuestasInquietudes.services', [

    ]).factory('respuestasInquietudesServices', respuestasInquietudesServices)
      ;

    respuestasInquietudesServices.$inject = ['$resource','BASEURL'];

    function respuestasInquietudesServices($resource,BASEURL) {


       return $resource(BASEURL+'/respuestasinquietudes/:id',
       {id:'@id'},
       {
        update:{
          url:BASEURL+'/respuestasinquietudes/:id',
          method:'PUT',
          params:{
            id:'@id'
          }
        },
        byIdInquietud:{
          url:BASEURL+'/respuestasinquietudes/byInquietudesId/:id',
          method:'GET',
          params:{
            id:'@id'
          }
        }
       });

    }

   

})();
/*
params:{
             adminJardin:'@adminJardin'
           }
           */
