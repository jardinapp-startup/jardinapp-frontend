(function () {
    'use strict';

    angular.module('app.respuestasInquietudes', [
      'app.respuestasInquietudes.router',
        'app.respuestasInquietudes.directivas',
        'app.respuestasInquietudes.controller',
        'app.respuestasInquietudes.services'  
    ]);

})();
