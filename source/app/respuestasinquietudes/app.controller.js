
(function () {
    'use strict';

    angular.module('app.respuestasInquietudes.controller', [
    ])
    .controller('respuestasInquietudesCtrl', respuestasInquietudesCtrl)
    .controller('respuestasInquietudesCreateCtrl', respuestasInquietudesCreateCtrl)
   ;

    respuestasInquietudesCtrl.$inject = ['respuestasInquietudesServices','$auth','$stateParams'];
    respuestasInquietudesCreateCtrl.$inject = ['respuestasInquietudesServices','$auth','$stateParams','$location','$mdToast'];

    function respuestasInquietudesCtrl(respuestasInquietudesServices,$auth,$stateParams) {
        var vm=this;
        vm.getCurrentId=getCurrentId;
        vm.isAuthenticated=isAuthenticated;

        vm.respuestasInquietudes = respuestasInquietudesServices.byIdInquietud({id:$stateParams.id});

        console.log(vm.respuestasInquietudes)
  //**********
        function getCurrentId(){
          if(isAuthenticated()){
            return $auth.getPayload().sub;
          }else{
            return;
          }
        }


    function isAuthenticated(){
          return $auth.isAuthenticated();
        }
  }
  function respuestasInquietudesCreateCtrl(respuestasInquietudesServices,$auth,$stateParams,$location,$mdToast) {
        var vm=this;
        vm.respuesta={
          //"idrespuestas":1,
          "respuesta":"",
          "idInquietudes":{
          "idInquietud":null
       }
        }
      vm.create=function(){
        vm.respuesta.idInquietudes.idInquietud=$stateParams.id;
        respuestasInquietudesServices.save(vm.respuesta).$promise
        .then(function(data){
          $mdToast.show(
              $mdToast.simple()
              .textContent('Respuesta enviada')
              .position('bottom right'));
          $location.path('/inquietudes');
        })

      };  
    /*
    Formato para create
    {
      "idrespuestas":1,
      "respuesta":"Si, esta semana hay reunion",
      "idInquietudes":{
      "idInquietud":"2"
       }
      }

    */    
  }
})();
