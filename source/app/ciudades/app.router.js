(function () {
    'use strict';

    angular.module('app.ciudades.router', [

    ])
        .config(configure);

   
    configure.$inject = ['$stateProvider', '$urlRouterProvider'];

    function configure($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('ciudades', {
                url: '/ciudades',
                views: {
                    'ciudades': {
                        template: '<ciudades/>'
                    },
                    'piepagina': {
                        template: '<piepagina/>'
                    },
                    'encabezado': {
                        template: '<encabezado/>'
                    }
                }
            });
    };
})();
