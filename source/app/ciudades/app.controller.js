(function () {
    'use strict';

    angular.module('app.ciudades.controller', [
    ])
    .controller('ciudadesCtrl', ciudadesCtrl);

   ciudadesCtrl.$inject = ['ciudadesServices'];

    function ciudadesCtrl(ciudadesServices) {
        this.ciudades = ciudadesServices.query();
    }

})();    