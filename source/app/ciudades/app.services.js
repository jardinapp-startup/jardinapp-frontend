(function () {
    'use strict';

    angular.module('app.ciudades.services', [

    ])
        .factory('ciudadesServices', ciudadesServices);

    ciudadesServices.$inject = ['$resource','BASEURL'];

    function ciudadesServices($resource,BASEURL) {
       return $resource(BASEURL+'/ciudades/:id');
    }

})();
