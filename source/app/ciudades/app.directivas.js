(function () {
    'use strict';

    angular.module('app.ciudades.directivas', [

    ]).directive('ciudades', ciudades);

    function ciudades() {
        return {
            scope: {},
            templateUrl: 'app/ciudades/lista.html',
            controller: 'ciudadesCtrl',
            controllerAs: 'vm'
        }
    }

})();
