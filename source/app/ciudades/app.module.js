(function () {
    'use strict';

    angular.module('app.ciudades', [
        'app.ciudades.router',
        'app.ciudades.directivas',
        'app.ciudades.controller',
        'app.ciudades.services'
    ]);

})();
