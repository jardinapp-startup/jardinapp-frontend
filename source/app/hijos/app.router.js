(function(){
    'use strict';
    angular.module('app.hijos.router',[
    ])
    .config(configure);

    configure.$inject = ['$stateProvider', '$urlRouterProvider'];

    function configure($stateProvider, $urlRouterProvider) {

        //$urlRouterProvider.otherwise('/');
        $stateProvider
            .state('hijoslist', {
                url: '/hijos',
                template: '<hijoslist/>'
            })
            .state('hijoslistpadre', {
                url: '/hijos/list',
                template: '<hijoslistpadre/>'
            })
            .state('hijoscreate', {
                url: '/hijos/create',
                template: '<hijoscreate/>'
            })
            .state('hijoscreateadmin', {
                url: '/hijos/create/admin',
                template: '<hijos-create-admin-jardin/>'
            })
            .state('hijosupdate', {
                url: '/hijos/update/:idHijo',
                template: '<hijosupdate/>'
            })
            .state('hijoslistadmina', {
              url: '/hijos/listadmin',
              template: '<hijoslistadmin/>'
            });

    };
})();
