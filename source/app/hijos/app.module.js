(function(){
    'use strict';
    angular.module('app.hijos',[
      'app.hijos.router',
      'app.hijos.services',
      'app.hijos.controller',
      'app.hijos.directivas'
    ]);
})();
