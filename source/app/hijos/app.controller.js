(function(){
  'use strict';

  angular.module('app.hijos.controller',[
  ]).controller('hijosCtrl', hijosCtrl)
    .controller('hijosCreateCtrl', hijosCreateCtrl)
    .controller('hijosUpdateCtrl', hijosUpdateCtrl)
    .controller('hijosListAdminCtrl', hijosListAdminCtrl);


//List
hijosCtrl.$inject = ['hijosServices','$location','SecurityFilter'];
  function hijosCtrl(hijosServices,$location,SecurityFilter) {
    var vm=this;

      if(SecurityFilter.isUser()){
        vm.hijos = hijosServices.getMisHijos();

      }
        vm.isAdminJardin=function(){
          return SecurityFilter.isAdminJardin();
        }
        if(SecurityFilter.isAdminJardin() || SecurityFilter.isAdmin() ){

          vm.query = {
            limit: 5,
            page: 1
          };
          vm.hijos = hijosServices.query();
        }

  }

//Create
hijosCreateCtrl.$inject = ['$location', 'hijosServices','SecurityFilter','$mdToast','jardinesinfantilesServices','usuariosServices'];
  function hijosCreateCtrl($location, hijosServices, SecurityFilter,$mdToast,jardinesinfantilesServices,usuariosServices) {
    var vm=this;
    vm.valRegistro=false;
    vm.hijos={
      idUsuarios:{
        idUsuarios:null
      }
    };
    jardinesinfantilesServices.getMiJardin().$promise
   .then(function(data){
    vm.jardines = data;
  });
  if(!SecurityFilter.isAdminJardin() && !SecurityFilter.isUser()){
        $location.path('/');
      }
      vm.buscarPadre=function(){
        usuariosServices.findUsuarioByDocumentoHijos({documento:vm.criterioBusqueda}).$promise
        .then(function(data){
          vm.valRegistro=false;
          vm.padres=data;
          vm.hijos.idUsuarios.idUsuarios=vm.padres.idUsuarios;
        })
        .catch(function(err){
          vm.valRegistro=true;
          $mdToast.show(
            $mdToast.simple()
            .textContent('El número de ducumento ya se encuentra registrado'+err)
            .position('bottom right'));
        });
      };
      vm.create = function() {
        if(vm.valRegistro){
          $mdToast.show(
              $mdToast.simple()
              .textContent('Ingrese todos los campos en rojo')
              .position('bottom right'));
        }else{

          hijosServices.save(vm.hijos, function(){
            $mdToast.show(
                $mdToast.simple()
                .textContent('El niño fue creado exitosamente')
                .position('bottom right'));
            $location.path('/hijos');
        });
      }
    };

    vm.createAdminJardin = function() {
      if(vm.valRegistro){
        $mdToast.show(
            $mdToast.simple()
            .textContent('Ingrese todos los campos en rojo')
            .position('bottom right'));
      }else{

        hijosServices.createAdminJardin(vm.hijos, function(){
          $mdToast.show(
              $mdToast.simple()
              .textContent('El niño fue creado exitosamente')
              .position('bottom right'));
          $location.path('/jardinesinfantiles/listusuariosv/'+vm.hijos.idJardinInfantil.idJardinInfantil);
      });
    }
  };

    vm.validaRegistro=function(){
  hijosServices.validaRegistro({registroCivil:vm.hijos.registroCivil}).$promise
  .then(function(data){
    console.log(data);
    vm.valRegistro=false;

  })
  .catch(function(err){
    vm.valRegistro=true;
  });

    };
  }

//Update
hijosUpdateCtrl.$inject = ['$stateParams', '$location', 'hijosServices', 'SecurityFilter'];
  function hijosUpdateCtrl($stateParams, $location, hijosServices, SecurityFilter) {
    var vm=this;
    if(SecurityFilter.isAdminJardin() || SecurityFilter.isUser()|| SecurityFilter.isAdmin()){
      vm.hijos = hijosServices.get({idHijo:$stateParams.idHijo});
    }else{
      $location.path('/');
    }


    vm.update =function(){
      hijosServices.update(vm.hijos, function() {
        $location.path('/hijos');
      });
    }
}

hijosListAdminCtrl.$inject = ['hijosServices', 'SecurityFilter','$location'];
function hijosListAdminCtrl(hijosServices, SecurityFilter, $location) {
  var vm = this;
  /*
  if(!SecurityFilter.isAdmin()){
    $location.path('/');
  }
*/
  this.hijos = hijosServices.query();
  vm.onChange = function (id) {
      hijosServices.inhabilit({idHijo:id}).$promise
      .then(function(data){
        console.log(data);
      })
      .catch(function(err){
        console.log(err);
      });
  }
}

})();
