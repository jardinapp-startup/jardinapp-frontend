(function(){
    'use strict';
    angular.module('app.hijos.directivas',[

    ]).directive('hijoslist',  hijosList)
      .directive('hijoscreate',  hijosCreate)
      .directive('hijosCreateAdminJardin',  hijosCreateAdminJardin)
      .directive('hijoslistpadre',  hijosListPadre)
      .directive('hijosupdate',  hijosUpdate)
      .directive('hijoslistadmin', hijosListAdmin);

function hijosList(){
  return{
    scope:{},
    restrict: 'EA',
    templateUrl: 'app/hijos/lista.html',
    controller: 'hijosCtrl',
    controllerAs: 'vm'
  }
}
function hijosListPadre(){
  return{
    scope:{},
    restrict: 'EA',
    templateUrl: 'app/hijos/lista.html',
    controller: 'hijosCtrl',
    controllerAs: 'vm'
  }
}

function hijosCreate(){
  return{
    scope:{},
    restrict:'EA',
    templateUrl: 'app/hijos/createPadre.html',
    controller: 'hijosCreateCtrl',
    controllerAs: 'vm'
  }
}
function hijosCreateAdminJardin(){
  return{
    scope:{},
    restrict:'EA',
    templateUrl: 'app/hijos/create.html',
    controller: 'hijosCreateCtrl',
    controllerAs: 'vm'
  }
}

function hijosUpdate(){
  return{
    scope:{},
    restrict: 'EA',
    templateUrl: 'app/hijos/update.html',
    controller: 'hijosUpdateCtrl',
    controllerAs: 'vm'
  }
}

function hijosListAdmin() {
  return {
    scope: {},
    restrict: 'EA',
    templateUrl: 'app/hijos/listaAdmin.html',
    controller: 'hijosListAdminCtrl',
    controllerAs: 'vm'
  }
}


})();
