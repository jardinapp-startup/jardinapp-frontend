(function(){
    'use strict';

    angular.module('app.hijos.services',[

    ]).factory('hijosServices', hijosServices);

     hijosServices.$inject = ['$resource','BASEURL'];

      function hijosServices($resource,BASEURL) {
          return $resource(BASEURL+'/hijos/:idHijo',
      { idHijo: '@idHijo' },
      { 'update': {method: 'PUT'},


      inhabilit: {
        url: BASEURL+'/hijos/inhabilit/:idHijo',
        method: 'PUT',
        params: {idHijo: '@idHijo'}
      },
      getMisHijos: {
        url: BASEURL+'/hijos/getMisHijos',
        method: 'GET',
        isArray:true
      },
      validaRegistro:{
      url:BASEURL+'/hijos/validaRegistro/:registroCivil',
      method:'GET',
      params:{
        registroCivil:'@registroCivil'
        }
      },
      createAdminJardin:{
        url:BASEURL+'/hijos/createAdminJardin',
        method:'POST',
      }

     });
    }

})();
