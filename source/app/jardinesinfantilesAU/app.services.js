(function () {
    'use strict';

    angular.module('app.jardinesinfantilesau.services', [

    ])
        .factory('jardinesinfantilesauServices', jardinesinfantilesauServices);

    jardinesinfantilesauServices.$inject = ['$resource','BASEURL'];

    function jardinesinfantilesauServices($resource,BASEURL) {
       return $resource(BASEURL+'/jardinesinfantilesAU');
    }

})();
