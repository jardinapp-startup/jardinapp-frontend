(function() {
    'use strict';

    angular.module('app.jardinesinfantilesau.directivas',[
    ])
    .directive('jardinesinfantilesau', jardinesinfantilesAU);

    function jardinesinfantilesAU(){
      return {
        scope: {},
        restrict:'EA',
        templateUrl: 'app/jardinesinfantilesAU/list.html',
        controller: 'jardinesinfantilesauCtrl',
        controllerAs: 'vm'
      }
    }
})();
