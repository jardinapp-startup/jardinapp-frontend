(function() {
    'use strict';

    angular.module('app.jardinesinfantilesau.controller', [
    ])
    .controller('jardinesinfantilesauCtrl', jardinesinfantilesauCtrl);

    jardinesinfantilesauCtrl.$inject = ['jardinesinfantilesauServices'];

    function jardinesinfantilesauCtrl(jardinesinfantilesauServices){
      this.jardinesinfantilesau = jardinesinfantilesauServices.query();
    }
})();
