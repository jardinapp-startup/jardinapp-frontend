(function(){
    'use strict';

    angular.module('app.jardinesinfantilesau.router', [
    ])
        .config(configure);

    configure.$inject = ['$stateProvider', '$urlRouterProvider'];

    function configure($stateProvider, $urlRouterProvider) {

      $urlRouterProvider.otherwise('/');

      $stateProvider
          .state('jardinesinfantilesau', {
              url: '/jardinesinfantilesau',
              template: '<jardinesinfantilesau/>'
          })
    };
})();
