(function () {
    'use strict';

    angular.module('app.jardinesinfantilesau', [
        'app.jardinesinfantilesau.router',
        'app.jardinesinfantilesau.directivas',
        'app.jardinesinfantilesau.controller',
        'app.jardinesinfantilesau.services'
    ]);

})();
