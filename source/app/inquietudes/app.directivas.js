
(function () {
    'use strict';

    angular.module('app.inquietudes.directivas', [

    ]).directive('inquietudes', inquietudes)
      .directive('misinquietudes', misinquietudes)
      .directive('inquietudescreate', inquietudesCreate);

    function inquietudes() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/inquietudes/lista.html',
            controller: 'inquietudesCtrl',
            controllerAs: 'vm'
        }
    }
//*******New
    function misinquietudes() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/inquietudes/misinquietudes.html',
            controller: 'misInquietudesCtrl',
            controllerAs: 'vm'
        }
    }
//***********
    function inquietudesCreate() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: 'app/inquietudes/create.html',
            controller: 'inquietudesCreateCtrl',
            controllerAs: 'vm'
        }
    }

})();
