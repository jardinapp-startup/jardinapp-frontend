
(function () {
    'use strict';

    angular.module('app.inquietudes.router', [

    ])
        .config(configure);

    //Se inyecta los parametros
    configure.$inject = ['$stateProvider', '$urlRouterProvider'];

    //Se configura las rutas de la aplicación para modelo
    function configure($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('inquietudes', {
                url: '/inquietudes',
                template: '<inquietudes/>'
            })
            .state('misinquietudes', {
                url: '/misinquietudes',
                template: '<misinquietudes/>'
            })
              .state('inquietudes-create', {
                  url: '/inquietudes/create/:idJardin',
                  template: '<inquietudescreate/>'
              });
    };
})();
