(function () {
    'use strict';

    angular.module('app.inquietudes', [
        'app.inquietudes.router',
        'app.inquietudes.directivas',
        'app.inquietudes.controller',
        'app.inquietudes.services'
    ]);

})();
