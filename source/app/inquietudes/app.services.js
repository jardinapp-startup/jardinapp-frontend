
(function () {
    'use strict';

    angular.module('app.inquietudes.services', [

    ]).factory('inquietudesServices', inquietudesServices)
      .factory('inquietudes', inquietudes);

    inquietudesServices.$inject = ['$resource','BASEURL'];

    function inquietudesServices($resource,BASEURL) {


       return $resource(BASEURL+'/inquietudes/byuser');

    }

    inquietudes.$inject = ['$resource','BASEURL'];

    function inquietudes($resource,BASEURL) {
       return $resource(BASEURL+'/inquietudes/:id',
       {id: '@id'},
     {'update': {method: 'PUT'},
       searchbyusuarios:{
         url: BASEURL+'/inquietudes/byusuarios',
         method: 'GET',
         isArray: true
       }
      });
    }

})();
/*
params:{
             adminJardin:'@adminJardin'
           }
           */
