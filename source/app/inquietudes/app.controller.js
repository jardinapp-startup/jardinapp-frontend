
(function () {
    'use strict';

    angular.module('app.inquietudes.controller', [
    ])
    .controller('inquietudesCtrl', inquietudesCtrl)
    .controller('misInquietudesCtrl', misInquietudesCtrl)
    .controller('inquietudesCreateCtrl', inquietudesCreateCtrl);

    inquietudesCtrl.$inject = ['inquietudesServices','$auth'];

    function inquietudesCtrl(inquietudesServices,$auth) {
        var vm=this;

        vm.inquietudes = inquietudesServices.query();
        
        vm.getCurrentId=getCurrentId;
        vm.isAuthenticated=isAuthenticated;

  //**********
        function getCurrentId(){
          if(isAuthenticated()){
            return $auth.getPayload().sub;
          }else{
            return;
          }
        }


    function isAuthenticated(){
          return $auth.isAuthenticated();
        }
  }
//Aqui inicia el controler para mis inquietudes
  misInquietudesCtrl.$inject = ['inquietudes','SecurityFilter','$location'];

  function misInquietudesCtrl(inquietudes, SecurityFilter, $location) {
    var vm = this;
      vm.inquietud = inquietudes.searchbyusuarios();
}
//Aqui termina

    inquietudesCreateCtrl.$inject = ['$location','inquietudes', '$stateParams','$mdToast'];

    function inquietudesCreateCtrl($location, inquietudes, $stateParams,$mdToast) {
      this.inquietudes = {
        asunto: null,
        idJardinInfantil:{
          idJardinInfantil: $stateParams.idJardin
        }
      };
        this.create = function() {
          inquietudes.save(this.inquietudes, function(){
            $mdToast.show(
              $mdToast.simple()
              .textContent('La inquietud fue creada exitosamente')
              .position('bottom right'));
            $location.path('/jardinesinfantiles');
          });
        }
    }
})();
