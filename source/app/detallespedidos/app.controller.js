(function () {
    'use strict';

    angular.module('app.detallespedidos.controller', [

    ]).controller('detallespedidosCtrl', detallespedidosCtrl)
      .controller('detallespedidosCreate', detallespedidosCreate)
      .controller('misPedidosCtrl', misPedidosCtrl);

    //List
    detallespedidosCtrl.$inject = ['detallespedidosServices','SecurityFilter','$location'];

    function detallespedidosCtrl(detallespedidosServices, SecurityFilter, $location) {
      var vm = this;
      if(SecurityFilter.isAdmin()){
        vm.detallespedidos = detallespedidosServices.query();
      }else {
      $location.path('/');
    }
}

    misPedidosCtrl.$inject = ['detallespedidosServices','SecurityFilter','$location', 'pedidosServices', '$mdToast'];

    function misPedidosCtrl(detallespedidosServices, SecurityFilter, $location, pedidosServices, $mdToast) {
      var vm = this;
      if(SecurityFilter.isUser() || SecurityFilter.isAdminJardin() || SecurityFilter.isEmpresario()){
        vm.detallespedidos = detallespedidosServices.searchbyusuario();
      }else {
      $location.path('/');
    }
    vm.onChange = function (id) {
        pedidosServices.inhabilit({idPedido:id}).$promise
        .then(function(data){
          $mdToast.show(
            $mdToast.simple()
            .textContent('Se actualizo correctamente')
            .position('bottom right'));
        })
        .catch(function(data){
        });
    }
}

    //Create
    detallespedidosCreate.$inject = ['$location',  'detallespedidosServices', 'pedidosServices', 'serviciosServices', '$stateParams', '$mdToast'];
    function detallespedidosCreate( $location, detallespedidosServices, pedidosServices, serviciosServices, $stateParams, $mdToast) {
      var vm = this;
      vm.fecha = new Date();//Con el Date toma la fecha actual
      vm.fecha.getDate();
      vm.resultado = null;
      vm.servicios = serviciosServices.get({idServicio: $stateParams.idServicio}).$promise.then(function (data){
      vm.banner = {};//Iniciarlizar la variable, solo para mostrar la informacion de los campos
      vm.banner.precio = data.precio;
      vm.banner.nombre = data.nombre;//Iniciarlizar la variable, solo para mostrar la informacion del campo Duracion
      console.log(vm.banner);
    });
      //Se realiza una function para calcular el total, de acuerdo a la cantidad que el usuario ingrese
      function operacion(){
        vm.resultado = vm.detalle.duracionMeses*vm.banner.precio;
        return vm.resultado;//y se retorna la variable para que muestre el total//
      }
      //Se inicializa una variable y se iguala con una funcion
      //que de acuerdo con la cantidad que ingrese el usuario,
      //le  coloque la fecha fin en la cual se teminará el servicio
      vm.detalle = {};
      vm.calcular = function (){
        operacion();
        vm.detalle.fechaFin = new Date();
        vm.detalle.fechaFin.setFullYear(vm.detalle.fechaFin.getFullYear(), (vm.detalle.fechaFin.getMonth() + (vm.detalle.duracionMeses)));
      };

      vm.create = function() {
        vm.pedidos = {};
        vm.pedidos.fecha = vm.fecha;//Se llama la variable para que sea almacenada en la BD
        vm.detalle.fechaInicio = vm.fecha;
        pedidosServices.save(vm.pedidos).$promise.then(function (data){
          console.log(data.idPedido);
          vm.detalle.detallesPedidosPK={};
          vm.detalle.detallesPedidosPK.idServicio = $stateParams.idServicio;
          vm.detalle.detallesPedidosPK.idPedido = data.idPedido;
          vm.detalle.pedidos = {idPedido: data.idPedido};
          vm.detalle.servicios = {idServicio: $stateParams.idServicio};
          console.log(vm.detalle);
          detallespedidosServices.save(vm.detalle);
          $mdToast.show(
            $mdToast.simple()
            .textContent('El pedido fue creado exitosamente')
            .position('bottom right'));
            $location.path('/mispedidos');
        });
      };
  }

})();
