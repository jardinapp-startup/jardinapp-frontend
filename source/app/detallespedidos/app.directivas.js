(function () {
    'use strict';

    angular.module('app.detallespedidos.directivas', [

    ]).directive('detallespedidos', detallespedidos)
    .directive('mispedidos', mispedidos)
    .directive('detallespedidoscreate', detallespedidosCreate);

    function detallespedidos() {
      return {
        scope: {},
        restrict: 'EA',
        templateUrl: 'app/detallespedidos/list.html',
        controller: 'detallespedidosCtrl',
        controllerAs: 'vm'
      }
    }
    function mispedidos() {
      return {
        scope: {},
        restrict: 'EA',
        templateUrl: 'app/detallespedidos/mispedidos.html',
        controller: 'misPedidosCtrl',
        controllerAs: 'vm'
      }
    }
    function detallespedidosCreate() {
      return {
        scope: {},
        restrict: 'EA',
        templateUrl: 'app/detallespedidos/create.html',
        controller: 'detallespedidosCreate',
        controllerAs: 'vm'
      }
    }
})();
