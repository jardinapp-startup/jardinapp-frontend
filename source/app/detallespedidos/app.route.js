(function () {
    'use strict';

    angular.module('app.detallespedidos.router', [

    ]).config(configure);

    configure.$inject = ['$stateProvider', '$urlRouterProvider'];

    function configure($stateProvider, $urlRouterProvider) {

      $urlRouterProvider.otherwise('/');

      $stateProvider
          .state('detallespedidos', {
            url: '/detallespedidos',
                template: '<detallespedidos/>'
          })
          .state('mispedidos', {
            url: '/mispedidos',
                template: '<mispedidos/>'
          })
          .state('detallespedidoscreate', {
            url: '/detallespedidos/create/:idServicio',
                template: '<detallespedidoscreate/>'
          })
    };
})();
