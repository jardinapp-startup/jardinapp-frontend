(function(){
  'use strict';

  angular.module('app.detallespedidos.services',[

  ]).factory('detallespedidosServices', detallespedidosServices)
    .factory('pedidosServices', pedidosServices);

  detallespedidosServices.$inject = ['$resource', 'BASEURL'];

  function detallespedidosServices($resource, BASEURL) {
    return $resource(BASEURL+'/detallespedidos/:id',
      {id: '@id'},
  {'update': {method: 'PUT'},
    searchbyusuario:{
      url: BASEURL+'/detallespedidos/byusuario',
      method: 'GET',
      isArray: true
    }
    });
  }

  pedidosServices.$inject = ['$resource','BASEURL'];

  function pedidosServices($resource,BASEURL) {
     return $resource(BASEURL+'/pedidos/:idPedido',
     { idPedido: '@idPedido' },
     { 'update': {method: 'PUT'},
     inhabilit: {
       url: BASEURL+'/pedidos/inhabilit/:idPedido',
       method: 'PUT',
       params: {idPedido: '@idPedido'}
     }
   });
  }
})();
