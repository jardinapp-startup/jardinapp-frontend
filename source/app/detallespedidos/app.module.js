(function () {
    'use strict';

    angular.module('app.detallespedidos', [
        'app.detallespedidos.directivas',
        'app.detallespedidos.controller',
        'app.detallespedidos.services',
        'app.detallespedidos.router'
    ]);

})();
