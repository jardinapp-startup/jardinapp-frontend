(function () {
    'use strict';

    angular.module('app.hijosau', [
        'app.hijosau.router',
        'app.hijosau.directivas',
        'app.hijosau.services',
        'app.hijosau.controller'
    ]);

})();
