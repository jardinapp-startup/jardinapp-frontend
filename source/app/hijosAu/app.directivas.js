(function() {
    'use strict';

    angular.module('app.hijosau.directivas',[
    ])
    .directive('hijosau', hijosAU);

    function hijosAU(){
      return {
        scope: {},
        restrict:'EA',
        templateUrl: 'app/hijosAu/lista.html',
        controller: 'hijosauCtrl',
        controllerAs: 'vm'
      }
    }
})();
