(function () {
    'use strict';

    angular.module('app.hijosau.services', [

    ])
        .factory('hijosauServices', hijosauServices);

    hijosauServices.$inject = ['$resource','BASEURL'];

    function hijosauServices($resource,BASEURL) {
       return $resource(BASEURL+'/hijosAu');
    }

})();
