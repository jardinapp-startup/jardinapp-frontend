(function() {
    'use strict';

    angular.module('app.hijosau.controller', [
    ])
    .controller('hijosauCtrl', hijosauCtrl);

    hijosauCtrl.$inject = ['hijosauServices'];

    function hijosauCtrl(hijosauServices){
      this.hijosau = hijosauServices.query();
    }
})();
