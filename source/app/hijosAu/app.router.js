(function(){
    'use strict';

    angular.module('app.hijosau.router', [
    ])
        .config(configure);

    configure.$inject = ['$stateProvider', '$urlRouterProvider'];

    function configure($stateProvider, $urlRouterProvider) {

      $urlRouterProvider.otherwise('/');

      $stateProvider
          .state('hijosau', {
              url: '/hijosau',
              template: '<hijosau/>'
          })
    };
})();
