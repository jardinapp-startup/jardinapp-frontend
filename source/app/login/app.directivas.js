(function () {
    'use strict';

    angular.module('app.login.directivas', [

    ])
    .directive('login', login)


    function login() {
        return {
            scope: {},
            restrict:'EA',
            templateUrl: 'app/login/login.html',
            controller:'loginCtrl',
            controllerAs:'vm'
        }
    }

})();
