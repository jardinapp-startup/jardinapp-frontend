(function(){
  'use strict';
    angular.module('app.login',[
    	'app.login.router',
        'app.login.directivas',
        'app.login.controller',
        'app.login.service',
        'app.config'
    ])
})();
