(function(){
  'use strict';
    angular.module('app.login.controller',[
      'satellizer'
    ])
    .controller('loginCtrl',loginCtrl)


    loginCtrl.$inject=['$location','$mdToast','SecurityFilter','$auth', 'usuariosServices'];


    function loginCtrl($location,$mdToast,SecurityFilter ,$auth, usuariosServices){

      var vm=this;
      if(SecurityFilter.isAuthenticated()){
        vm.usuario = usuariosServices.get({idUsuarios: SecurityFilter.getCurrentUserId()});
      }
      
      vm.signIn=function(){
          SecurityFilter.signIn(vm.user);
          //$location.path('/');
          vm.user={};
        };
        
     

      vm.logout=function(){
          SecurityFilter.logOut();
          $location.path('/');
        }

      vm.isAuthenticated=function(){
          return SecurityFilter.isAuthenticated();
        }

      vm.isAdmin=function(){
        return SecurityFilter.isAdmin();
        }
      vm.isUser=function(){
        return SecurityFilter.isUser();
      }
      vm.isAdminJardin=function(){
        return SecurityFilter.isAdminJardin();
      }
      vm.isEmpresario=function(){
        return SecurityFilter.isEmpresario();
      }
      vm.getCurrentUser=function(){
          return SecurityFilter.getCurrentUser();
        }
      vm.getCurrentId=function(){
        return SecurityFilter.getCurrentUserId();
      }

    } /** fin loginCtrl **/
    /*
    */
})();
