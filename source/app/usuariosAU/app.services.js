(function () {
    'use strict';

    angular.module('app.usuariosau.services', [

    ])
        .factory('usuariosauServices', usuariosauServices);

    usuariosauServices.$inject = ['$resource', 'BASEURL'];

    function usuariosauServices($resource, BASEURL) {
       return $resource(BASEURL+'/usuariosAU');
    }

})();
