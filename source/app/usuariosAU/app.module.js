(function() {
  'use strict';

  angular.module('app.usuariosau', [
    'app.usuariosau.router',
    'app.usuariosau.directivas',
    'app.usuariosau.controller',
    'app.usuariosau.services'
  ]);

})();
