(function(){
  'use strict';

  angular.module('app.usuariosau.controller', [
  ])
    .controller('usuariosauCtrl',usuariosauCtrl);

    usuariosauCtrl.$inject = ['usuariosauServices'];

    function usuariosauCtrl(usuariosauServices){
      this.usuariosau = usuariosauServices.query();
}

})();
