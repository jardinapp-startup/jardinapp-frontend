(function(){
    'use strict';

    angular.module('app.usuariosau.router', [
    ])
        .config(configure);

    configure.$inject = ['$stateProvider', '$urlRouterProvider'];

    function configure($stateProvider, $urlRouterProvider) {

      $urlRouterProvider.otherwise('/');

      $stateProvider
          .state('usuariosau', {
              url: '/usuariosau',
              template: '<usuariosau/>'
          })
    };
})();
