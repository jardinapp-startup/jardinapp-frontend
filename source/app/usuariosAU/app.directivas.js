(function() {
    'use strict';

    angular.module('app.usuariosau.directivas',[
    ])
    .directive('usuariosau', usuariosAU);

    function usuariosAU(){
      return {
        scope: {},
        restrict:'EA',
        templateUrl: 'app/usuariosAU/list.html',
        controller: 'usuariosauCtrl',
        controllerAs: 'vm'
      }
    }
})();
