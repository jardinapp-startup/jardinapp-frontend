(function() {
  'use strict';
  angular.module('app.quienessomos.router', [

  ])
  .config(configure);

  configure.$inject = ['$stateProvider', '$urlRouterProvider'];

  function configure($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('quienessomos', {
          url: '/quienessomos',
            template: '<quienessomos/>'
        })
    };
})();
