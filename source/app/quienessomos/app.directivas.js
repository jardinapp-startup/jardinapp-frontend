(function () {
    'use strict';

    angular.module('app.quienessomos.directivas', [

    ]).directive('quienessomos', quienessomos);

    function quienessomos() {

         return {
            scope: {},
            restrict:'EA',
            templateUrl: 'app/quienessomos/quienessomos.html',
            controller:'quienesSomosCtrl',
            controllerAs:'vm'
        }
    }
})();
