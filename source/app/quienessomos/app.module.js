(function() {
  'use strict';

  angular.module('app.quienessomos', [
    'app.quienessomos.controller',
    'app.quienessomos.directivas',
    'app.quienessomos.router'
  ]);
})();
