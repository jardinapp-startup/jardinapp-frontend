(function () {
    'use strict';

    angular.module('app.inicio', [
        'app.inicio.services',
        'app.inicio.controller',
        'app.inicio.directivas',
        'app.inicio.router'
    ]);

})();