(function () {
    'use strict';

    angular.module('app.inicio.services', [

    ])
        .factory('jardinesService', jardinesService)
        .factory('inicioService', inicioService);

    jardinesService.$inject = ['$resource','BASEURL'];
    inicioService.$inject = ['$resource','BASEURL'];


    //Este servicio nos provee de los métodos GET POST PUT DELETE
    function jardinesService($resource,BASEURL) {
       return $resource(BASEURL+'/jardinesinfantiles/:id');
    }

    function inicioService($resource,BASEURL){

      return $resource(BASEURL+'/usuarios/contacto');
    }
    //De igual manera los factory nos sirven para almacenar información
    //y que nos pueda servir en cualquier controlador o lugar de la aplicación
    //evitando de esta manera hacer variables globales.

})();
