(function () {
    'use strict';

    angular.module('app.inicio.controller', [
    ])
    .controller('inicioCtrl', inicioCtrl)
    inicioCtrl.$inject=['inicioService','$mdToast','$location'];
    function inicioCtrl(inicioService,$mdToast,$location) {
    	var vm=this;
      vm.contactar=function(){
    	   inicioService.save(vm.contacto).$promise
         .then(function(data){
           $mdToast.show(
             $mdToast.simple()
             .textContent('Mensaje enviado Correctamente')
             .position('top right'));
         });
         $location.path('/');
         vm.contacto={};
      }
    }

})();
