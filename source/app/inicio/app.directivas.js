(function () {
    'use strict';

    angular.module('app.inicio.directivas', [

    ])
    .directive('inicio', inicio)
    .directive('cuentas', cuentas)
    .directive('juegos', juegos)

    function inicio() {
    	 return {
            scope: {},
            restrict:'EA',
            templateUrl: 'app/inicio/inicio.html',
            'controller':'inicioCtrl',
            'controllerAs':'vm'
        }
    }
     function cuentas() {
         return {
            scope: {},
            restrict:'EA',
            templateUrl: 'app/inicio/cuentas.html',
            'controller':'inicioCtrl',
            'controllerAs':'vm'
        }
    }
    function juegos() {
         return {
            scope: {},
            restrict:'EA',
            templateUrl: 'app/inicio/juegos.html',
            'controller':'inicioCtrl',
            'controllerAs':'vm'
        }
    }

  
})();
