
(function () {
    'use strict';

    angular.module('app.inicio.router', [

    ])
        .config(configure);

    //Se inyecta los parametros
    configure.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

    //Se configura las rutas de la aplicación para modelo
    function configure($stateProvider, $urlRouterProvider, $locationProvider) {

        $urlRouterProvider.otherwise('/');
        //$locationProvider.html5Mode(true);

        $stateProvider
            .state('inicio', {
                url: '/',
                template: '<inicio/>'
            })
            .state('cuentas', {
                url: '/inicio/cuentas',
                template: '<cuentas/>'
            })
            .state('juegos', {
                url: '/juegos',
                template: '<juegos/>'
            })
            ;
    };
})();
