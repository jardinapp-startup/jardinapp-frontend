(function () {
    'use strict';

    angular.module('app.calificaciones', [
        'app.calificaciones.router',
        'app.calificaciones.directivas',
        'app.calificaciones.controller',
        'app.calificaciones.services'
    ]);

})();
