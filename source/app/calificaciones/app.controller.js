(function() {
    'use strict';

    angular.module('app.calificaciones.controller', [
    ])
    .controller('calificacionesCtrl', calificacionesCtrl)
    .controller('calificacionesCreate', calificacionesCreate);

    calificacionesCtrl.$inject = ['calificacionesByUserService'];

    function calificacionesCtrl(calificacionesByUserService){
      console.log("hola calificaciones");
      var vm=this;
      vm.calificaciones = calificacionesByUserService.query();
      console.log(this.calificaciones);
    }

    //Create
    calificacionesCreate.inject = ['$location', 'calificacionesServices', '$stateParams'];

    function calificacionesCreate($location, calificacionesServices, $stateParams) {
      this.calificaciones = {
        puntuacion: null,
        idJardinInfantil:{
          idJardinInfantil: $stateParams.idJardin
        }
      };
      console.log(this.calificaciones);

      this.create = function() {
        console.log(this.calificaciones);
        //this.calificaciones.idJardinInfantil.idJardinInfantil = $stateParams.idJardin;
        calificacionesServices.save(this.calificaciones, function(data){
          console.log(data);
          $location.path('/calificaciones');
        });
      };
  }
})();
