(function() {
    'use strict';

    angular.module('app.calificaciones.directivas',[
    ])
    .directive('calificaciones', calificaciones)
    .directive('calificacionescreate', calificacionesCreate);


    function calificaciones(){
      return {
        scope: {},
        restrict:'EA',
        templateUrl: 'app/calificaciones/list.html',
        controller: 'calificacionesCtrl',
        controllerAs: 'vm'
      }
    }

    function calificacionesCreate(){
      return {
        scope: {},
        restrict:'EA',
        templateUrl: 'app/calificaciones/create.html',
        controller: 'calificacionesCreate',
        controllerAs: 'vm'
      }
    }
})();
