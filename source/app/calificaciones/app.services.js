(function () {
    'use strict';

    angular.module('app.calificaciones.services', [

    ])
        .factory('calificacionesServices', calificacionesServices)
        .factory('calificacionesByUserService', calificacionesByUserService);


    calificacionesServices.$inject = ['$resource','BASEURL'];

    function calificacionesServices($resource,BASEURL) {
       return $resource(BASEURL+'/calificaciones/create');
    }

    calificacionesByUserService.$inject = ['$resource','BASEURL'];

    function calificacionesByUserService($resource,BASEURL) {
       return $resource(BASEURL+'/calificaciones/byuser');
    }

})();
