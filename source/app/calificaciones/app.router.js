(function(){
    'use strict';

    angular.module('app.calificaciones.router', [
    ])
        .config(configure);

    configure.$inject = ['$stateProvider', '$urlRouterProvider'];

    function configure($stateProvider, $urlRouterProvider) {

      $urlRouterProvider.otherwise('/');

      $stateProvider
          .state('calificaciones', {
              url: '/calificaciones',
              template: '<calificaciones/>'
          })

          .state('calificacionescreate', {
              url: '/calificaciones/create/:idJardin',
              template: '<calificacionescreate/>'
          })
    };
})();
