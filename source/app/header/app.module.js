(function () {
    'use strict';

    angular.module('app.header', [
        'app.header.directivas',
        'app.header.controller'

    ]);

})();
